<%--
  Created by IntelliJ IDEA.
  User: charis
  Date: 9/19/2017
  Time: 2:44 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="Beans.UserBean" %>
<%@ page import="Beans.SearchBean" %>
<%@ page import="java.util.ArrayList" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width-device-width, initial-scale1=1">
    <meta name="description" content="Vacation Rentals, Homes, Experiences & Places">
    <meta name="keywords" content="hospitality service, rent, lease, vacation rentals, apartment rentals, homestays, hostel, hostel beds, hotel rooms, experiences, places">
    <meta name="author" content="Babis Alexiou">

    <title>Host & Chill | Search Results</title>
    <link rel="shortcut icon"  href="/favicon.ico?" />

    <%
        SearchBean current_search = (SearchBean) session.getAttribute("current_search");
        UserBean info = (UserBean) session.getAttribute("Info");
        ArrayList results = current_search.getSearch_rs();
    %>

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="css/modal.css">
    <link rel="stylesheet" href="css/star.css">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css"/>
    <!-- Latest compiled and minified JavaScript -->
    <script src="js/jquery.js"></script>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootsrap.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Oxygen" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">

    <!-- Include Required Prerequisites -->

    <!-- Include Date Range Picker -->
    <script src="daterangepicker/moment.min.js"></script>
    <script src="daterangepicker/daterangepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="daterangepicker/daterangepicker.css" />
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">
<!-- Navbar starts here -->
<style>
    #brand_image {
        height: 30px;
    }

</style>

<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <%
            if (info != null) {
        %>
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="logged.jsp">
                <img id="brand_image" src="images/LOGO.jpg" alt="">
            </a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav navbar-right">
                <% String first_name = (String) session.getAttribute("username"); %>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Welcome, <%= first_name %> <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><img src="<%=info.getPicture()%>" id="profile_image" class=" img-circle" alt=""> <%= first_name %></li>
                        <li class="divider"></li>
                        <li><a href="DisplayUserServlet?username=<%=info.getUsername()%>"><i class="icon-cog"></i> Preferences</a></li>
                        <li><a href="#"><i class="icon-envelope"></i> Contact Support</a></li>
                        <li class="divider"></li>
                        <li><a href="LogoutServlet/"><i class="icon-off"></i> Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
        <%
            } else {
        %>
        <!-- Navigation Bar starts here -->
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="index.jsp">
                        <img id="brand_image" src="images/LOGO.jpg" alt="">
                    </a>
                </div>
                <div class="collapse navbar-collapse" id="myNavbar">
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="register.jsp">Sign Up</a></li>
                        <li><a href="#" data-toggle="modal" data-target="#login-modal">Log In</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <!-- Navigation Bar ends here -->

        <!-- Login Modal starts here -->
        <div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none">
            <div class="modal-dialog">
                <div class="loginmodal-container">
                    <h1 style="font-family: 'Camelia', cursive;">Login to your Account</h1>
                    <hr/>
                    <form action="LoginServlet/">
                        <input type="text" name="username" id="username" placeholder="Username">
                        <input type="password" name="password" id="password" placeholder="Enter Your Password">
                        <input type="submit" name="login" class="loginmodal-submit" value="Login">
                    </form>
                    <div class="login-help">
                        <a href="register.jsp">Register</a> - <a href="forgotPassword.jsp">Forgot Password</a>
                    </div>
                </div>
            </div>
        </div>
        <!-- End Modal Login -->
        <%
            }
        %>
    </div>
</nav>
<!-- Navbar ends here -->
<header>
    <section class="bg-info" style="font-family: 'Raleway', sans-serif;background-color: #feefed;">
        <div class="container-fluid">
            <div class="col-sm-2">
                <div id="filter_col6">
                    <table class=".table-hover" style="table-layout: auto">
                        <thead>
                        <tr>
                            <th style="text-align: center; font-size: x-large;">Search Filters</th>
                        </tr>
                        </thead>

                        <tbody>
                        <tr >
                            <td style="text-align: center; font-size: large;"><br>Amenities: </td>
                        </tr>
                        <tr >
                            <td style="text-align: right; font-size: medium;"><br>Wireless Internet <input class="column_filter" id="col6_wireless_internet" value="Wireless Internet" type="checkbox"></td>
                        </tr>
                        <tr>
                            <td style="text-align: right; font-size: medium;">Air Conditioning <input class="column_filter" id="col6_air_conditioning" value="Air Conditioning" type="checkbox"></td>
                        </tr>
                        <tr>
                            <td style="text-align: right; font-size: medium;">Heating <input class="column_filter" id="col6_heating" value="Heating" type="checkbox"></td>
                        </tr>
                        <tr>
                            <td style="text-align: right; font-size: medium;">Kitchen <input class="column_filter" id="col6_kitchen" value="Kitchen" type="checkbox"></td>
                        </tr>
                        <tr>
                            <td style="text-align: right; font-size: medium;">TV <input class="column_filter" id="col6_tv" value="TV" type="checkbox"></td>
                        </tr>
                        <tr>
                            <td style="text-align: right; font-size: medium;">Parking <input class="column_filter" id="col6_free_parking_on_premises" value="Free Parking on Premises" type="checkbox"></td>
                        </tr>
                        <tr>
                            <td style="text-align: right; font-size: medium;">Elevator <input class="column_filter" id="col6_elevator_in_building" value="Elevator in Building" type="checkbox"></td>
                        </tr>
                        <tr>
                            <br>
                            <td><input id="min" name="min" type="text" placeholder="Minimum Daily Fee"></td>
                            <br>
                        </tr>
                        <tr>
                            <br>
                            <td><input id="max" name="max" type="text" placeholder="Maximum Daily Fee"></td>
                            <br>
                        </tr>
                        <tr>
                            <td style="text-align: left; font-size: large;"><br>Select Room type: </td>
                        </tr>
                        <tr>
                            <td align="center" id="room_type_filter"></td>
                        </tr>

                        </tbody>
                    </table>

                </div>
            </div>

            <div class="col-sm-8 col-sm-offset-1" style="background-color: #feefed">
                <h4 align="center">Search results for <strong><%=current_search.getLocation()%> <%=current_search.getStartDate()%>/<%=current_search.getEndDate()%></strong></h4>
                <hr>
                <%
                    if (!results.isEmpty()) {
                %>

                <table id="items" class="table table-hover" style="table-layout: auto;">
                    <thead>
                    <tr>
                        <th></th>
                        <th style="text-align: center;">Daily fee</th>
                        <th style="text-align: center;">Room Type</th>
                        <th style="text-align: center;">Beds</th>
                        <th style="text-align: center;">Reviews</th>
                        <th>Rating</th>
                        <th style="text-align: center;">Amenities</th>
                    </tr>
                    </thead>
                    <%
                        for (int i = 0; i < results.size(); i++) {
                            ArrayList results2 = (ArrayList) results.get(i);
                            System.out.println(results2.get(0));
                    %>
                    <!--<a href="DisplayListingServlet?id=<%=results2.get(7)%>"> </a>-->
                    <tr class='clickable-row' data-href="DisplayListingServlet?id=<%=results2.get(7)%>">
                        <th style="width: 20%;"><img src="<%=results2.get(0)%>" class="img-thumbnail" style="object-fit: contain; width: 185px; height: 120px;"></th>
                        <th style="width: 20%; font-size: x-large; text-align: center;"><%=results2.get(1)%>$</th>
                        <th style="width: 20%; text-align: center;"><%=results2.get(2)%></th>
                        <th style="width: 10%; font-size: large; text-align: center;"><%=results2.get(3)%></th>
                        <th style="text-align: center;"><%=results2.get(4)%></th>
                        <th style="text-align: center;"><span class="stars"><%=results2.get(5)%></span></th>
                        <th><%=results2.get(6)%></th>

                    </tr>

                    <%
                        }
                    %>
                </table>
                <%} else {
                %>
                <h5 align="center">Oops! No results found :(</h5>
                <%
                    }
                %>

            </div>


        </div>

    </section>
</header>
<script>
    $.fn.stars = function() {
        return $(this).each(function() {
            $(this).html($('<span />').width(Math.max(0, (Math.min(5, parseFloat($(this).html())))) * 16));
        });
    };

    $(function() {
        $('span.stars').stars();
    });
</script>
<!-- jQuery -->
<script src="vendor/jquery/jquery.min.js"></script>
<script type="text/javascript" src=https://code.jquery.com/jquery-1.12.4.js></script>

<!-- Bootstrap Core JavaScript -->
<script src="vendor/bootstrap/js/bootstrap.min.js"></script>

<!-- Plugin JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
<script src="vendor/scrollreveal/scrollreveal.min.js"></script>
<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

<!-- Theme JavaScript -->
<script src="js/creative.min.js"></script>

<!-- DataTables JavaScript -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.15/datatables.min.css"/>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.15/datatables.min.js"></script>
<script type="text/javascript" src="js/searchResults.js"></script>

</body>
</html>
