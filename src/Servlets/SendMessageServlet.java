/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Beans.ListingBean;
import Beans.MessageBean;
import Beans.UserBean;
import DAOs.MessageDAO;
import DAOs.UserDAO;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class SendMessageServlet extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, java.io.IOException {

        try {
            MessageBean message = new MessageBean();
            UserBean user;
            String username;
            String body;
            Date date = new Date();
            ListingBean listing ;
                    
            DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String currentDate = dateFormat.format(date);

            HttpSession session = request.getSession(true);

            user = (UserBean) session.getAttribute("Info");
            body = request.getParameter("body");

            message.setFrom_user(user.getUsername());
            //message.setTo_user(hostBean.getUsername());
            message.setTo_user((String) (session.getAttribute("message_to")));
            message.setMessage(body);
            message.setIsseen(false);
            message.setDate(currentDate);

            MessageDAO.insertMessage(message);

            response.sendRedirect("MessageServlet");
        } catch (Throwable theException) {
            System.out.println("SendMessageServlet Failed!: " + theException);
        }

    }
    
}
