<%--
  Created by IntelliJ IDEA.
  User: charis
  Date: 9/21/2017
  Time: 12:08 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="Beans.UserBean" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width-device-width, initial-scale1=1">
    <meta name="description" content="Vacation Rentals, Homes, Experiences & Places">
    <meta name="keywords" content="hospitality service, rent, lease, vacation rentals, apartment rentals, homestays, hostel, hostel beds, hotel rooms, experiences, places">
    <meta name="author" content="Babis Alexiou">

    <title>Host & Chill | Edit my Info</title>
    <link rel="shortcut icon"  href="/favicon.ico?" />

    <%
        UserBean info = (UserBean) session.getAttribute("Info");
    %>



    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="css/register.css">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css"/>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Oxygen" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">

    <!-- Latest compiled and minified JavaScript -->
    <script src="js/jquery.js"></script>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootsrap.js"></script>
    <script src="js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <div class="row main">
        <div class="panel-heading">
            <div class="panel-title text-center">
                <h1 class="title"> Host & Chill </h1>
                <hr/>
                <h2 class="title">Edit my Information</h2>
            </div>
        </div>
        <div class="main-login main-center">
            <form name="update" action="UpdateUserServlet" method="post" class="form-horizontal" onsubmit="return validatePassword()" enctype="multipart/form-data">
                <!-- Form Fields Start Here -->
                <!-- First Name -->
                <div class="form-group">
                    <label for="register_first_name" class="cols-sm-2 control-label">First Name</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" name="first_name" id="register_first_name" placeholder="Enter your First Name" value="<%=info.getFirst_name()%>">
                        </div>
                    </div>
                </div>
                <!-- Last Name -->
                <div class="form-group">
                    <label for="register_last_name" class="cols-sm-2 control-label">Last Name</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" name="last_name" id="register_last_name" placeholder="Enter your Last Name" value="<%=info.getLast_name()%>">
                        </div>
                    </div>
                </div>
                <!-- Email -->
                <div class="form-group">
                    <label for="register_email" class="cols-sm-2 control-label">Email</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                            <input type="email" class="form-control" name="email" id="register_email" placeholder="Enter your Email" value="<%=info.getEmail()%>">
                        </div>
                    </div>
                </div>
                <!-- Phone Number -->
                <div class="form-group">
                    <label for="register_phonenumber" class="cols-sm-2 control-label">Phone Number</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-phone fa" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" name="PhoneNumber" id="register_phonenumber" placeholder="Enter your Phone Number" value="<%=info.getPhonenumber()%>">
                        </div>
                    </div>
                </div>
                <!-- Username -->
                <div class="form-group">
                    <label for="register_username" class="cols-sm-2 control-label">Username</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-users fa" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" name="username" id="register_username" placeholder="Enter your Username" value="<%=info.getUsername()%>">
                        </div>
                    </div>
                </div>
                <!-- Password -->
                <div class="form-group">
                    <label for="register_password" class="cols-sm-2 control-label">Password</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-lock fa" aria-hidden="true"></i></span>
                            <input type="password" class="form-control" name="password" id="register_password" placeholder="Enter your Password">
                        </div>
                    </div>
                </div>
                <!-- Repeat Passoword -->
                <div class="form-group">
                    <label for="register_repeat_password" class="cols-sm-2 control-label">Repeat Password</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-unlock fa" aria-hidden="true"></i></span>
                            <input type="password" class="form-control" name="rpassword" id="register_repeat_password" placeholder="Repeat your Password">
                        </div>
                    </div>
                </div>
                <!-- Picture URL -->
                <div class="form-group">
                    <label for="register_picture_url" class="cols-sm-2 control-label">Picture (URL)</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-link fa" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" name="PictureURL" id="register_picture_url" placeholder="Enter your URL">
                        </div>
                    </div>
                </div>
                <!-- Picture File -->
                <div class="form-group">
                    <label for="register_picture_file" class="cols-sm-2 control-label">Picture (File)</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <input type="file" name="PictureFile" id="register_picture_file">
                        </div>
                    </div>
                </div>
                <!-- Host Role -->
                <div class="form-group">
                    <label for="register_host_role" class="cols-sm-2 control-label">I want to become a host</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <input type="checkbox" name="isHost" id="register_host_role" value="true">
                        </div>
                    </div>
                </div>
                <!-- Renter Role -->
                <div class="form-group">
                    <label for="register_renter_role" class="cols-sm-2 control-label">Looking for a place to stay</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <input type="checkbox" name="isRenter" id="register_renter_role" value="true" required>
                        </div>
                    </div>
                </div>
                <!-- Form Fields End Here -->
                <!-- Submit Button -->
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-lg btn-block login-button">Submit</button>
                </div>
                <!-- Login Link -->
                <div class="login-register">
                    <a href="user.jsp">Back</a>
                </div>
            </form>
        </div>
    </div>
</div>
</body>
</html>
