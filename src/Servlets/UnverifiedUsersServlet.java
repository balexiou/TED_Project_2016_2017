package Servlets;

import DAOs.UnverifiedUsersDAO;
import Beans.UnverifiedUsersBean;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by Babis on 9/13/2017.
 */
@WebServlet(name = "UnverifiedUsersServlet")
public class UnverifiedUsersServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {

            UnverifiedUsersBean unverifiedUsers = new UnverifiedUsersBean();
            unverifiedUsers = UnverifiedUsersDAO.unverifiedUsers(unverifiedUsers);

            HttpSession session = request.getSession(true);
            session.setAttribute("UnverifiedUsers", unverifiedUsers);

            response.sendRedirect("adminuserverify.jsp");

        } catch (Throwable thException) {
            System.out.println(thException);
        }

    }
}
