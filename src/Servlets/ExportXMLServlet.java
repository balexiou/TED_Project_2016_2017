package Servlets;

import DAOs.ExportDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by charis on 9/22/2017.
 */
@WebServlet(name = "ExportXMLServlet")
public class ExportXMLServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {

            int xml = 0;
            String file_name = null;
            String file_type = "text";
            xml = Integer.parseInt(request.getParameter("btn"));

            String root = getServletContext().getRealPath("/");
            File directory = new File(root + "/xmlExports");
            if (!directory.exists()) {
                directory.mkdir();
            }
            String path = directory.getAbsolutePath();
            System.out.println(path);

            File xml_file = null;
            switch (xml) {
                case 1:
                    ExportDAO.exportBookings(path);
                    xml_file = new File(path + "/bookings.xml");
                    if (xml_file.exists() && !xml_file.isDirectory()) {
                        response.setHeader("Content-disposition", "attachment; filename=bookings.xml");
                        System.out.println("I will download the bookings!");
                    }
                    break;
                case 2:
                    ExportDAO.exportListings(path);
                    xml_file = new File(path + "/listings.xml");
                    if (xml_file.exists() && !xml_file.isDirectory()) {
                        response.setHeader("Content-disposition", "attachment; filename=listings.xml");
                        System.out.println("I will download the listings!");
                    }
                    break;
                case 3:
                    ExportDAO.exportUsers(path);
                    xml_file = new File(path + "/users.xml");
                    if (xml_file.exists() && !xml_file.isDirectory()) {
                        response.setHeader("Content-disposition", "attachment; filename=users.xml");
                        System.out.println("I will download the users!");
                    }
                    break;
                default:
                    break;
            }

            response.setContentType(file_type);

            OutputStream outputStream = response.getOutputStream();
            FileInputStream inputStream = new FileInputStream(xml_file);
            byte[] buffer = new byte[4096];
            int length;
            while ((length = inputStream.read(buffer)) > 0) {
                outputStream.write(buffer, 0 , length);
            }
            inputStream.close();
            outputStream.flush();

            response.sendRedirect("admin.jsp");

        } catch (Throwable thException){
            System.out.println(thException);
        }

    }
}
