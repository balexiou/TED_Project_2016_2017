/**
 * Created by charis on 9/24/2017.
 */

$.fn.stars = function() {
    return $(this).each(function() {
        $(this).html($('<span />').width(Math.max(0, (Math.min(5, parseFloat($(this).html())))) * 16));
    });
};

$(function() {
    $('span.stars').stars();
});

jQuery.fn.dataTableExt.oSort['numeric-comma-asc']  = function(a,b) {
    var x = (a == "-") ? 0 : a.replace( /,/, "." );
    var y = (b == "-") ? 0 : b.replace( /,/, "." );
    x = parseFloat( x );
    y = parseFloat( y );
    return ((x < y) ? -1 : ((x > y) ?  1 : 0));
};

jQuery.fn.dataTableExt.oSort['numeric-comma-desc'] = function(a,b) {
    var x = (a == "-") ? 0 : a.replace( /,/, "." );
    var y = (b == "-") ? 0 : b.replace( /,/, "." );
    x = parseFloat( x );
    y = parseFloat( y );
    return ((x < y) ?  1 : ((x > y) ? -1 : 0));
};

function isEmpty(value) {
    return typeof value == 'string' && !value.trim() || typeof value == 'undefined' || value === null;
}

function filterColumn ( i ) {
    var search_term = '';
    var str1 = $('#col'+i+'_wireless_internet:checked').val();
    if(!isEmpty(str1)) search_term = search_term.concat(" ",str1);
    var str2 = $('#col'+i+'_air_conditioning:checked').val();
    if(!isEmpty(str2)) search_term = search_term.concat(" ",str2);
    var str3 = $('#col'+i+'_heating:checked').val();
    if(!isEmpty(str3)) search_term = search_term.concat(" ",str3);
    var str4 = $('#col'+i+'_kitchen:checked').val();
    if(!isEmpty(str4)) search_term = search_term.concat(" ",str4);
    var str5 = $('#col'+i+'_tv:checked').val();
    if(!isEmpty(str5)) search_term = search_term.concat(" ",str5);
    var str6 = $('#col'+i+'_free_parking_on_premises:checked').val();
    if(!isEmpty(str6)) search_term = search_term.concat(" ",str6);
    var str7 = $('#col'+i+'_elevator_in_building:checked').val();
    if(!isEmpty(str7)) search_term = search_term.concat(" ",str7);

    if(isEmpty(search_term)) console.info("search term empty");
    else console.info(search_term);
    $('#items').DataTable().column( i ).search( search_term
    ).draw();
}

$.fn.dataTable.ext.search.push(
    function( settings, data, dataIndex ) {
        var min = parseFloat( $('#min').val(), 10 );
        var max = parseFloat( $('#max').val(), 10 );
        var price = parseFloat( data[1] ) || 0; // use data for the price column

        if ( ( isNaN( min ) && isNaN( max ) ) ||
            ( isNaN( min ) && price <= max ) ||
            ( min <= price   && isNaN( max ) ) ||
            ( min <= price   && price <= max ) )
        {
            return true;
        }
        return false;
    }
);

$(document).ready(function() {
    var table = $('#items').DataTable({
        "searching": true,
        //"lengthChange": false,
        "columnDefs": [
            {
                "orderable": false,
                "targets": [0,2,5]
            },
            {
                "visible": false,
                "targets": [ 6 ],
                "searchable": true
            }
        ],
        "order": [[1, 'asc']],
        "aoColumns": [
            null,
            { "sType": "numeric-comma" },
            null,
            null,
            null,
            null,
            null
        ],
        "stateSave": false,
        "dom": '<"top"lp<"clear">>rt<"bottom"ip<"clear">>',

        initComplete: function () {
            this.api().columns([2]).every( function () {
                var column = this;
                var select = $('<select><option value="">Show All</option></select>')
                    .appendTo( $('#room_type_filter').empty() )
                    .on( 'change', function () {
                        var val = $.fn.dataTable.util.escapeRegex(
                            $(this).val()
                        );

                        column
                            .search( val ? '^'+val+'$' : '', true, false )
                            .draw();
                    } );

                column.data().unique().sort().each( function ( d, j ) {
                    select.append( '<option value="'+d+'">'+d+'</option>' )
                } );
            } );
        }

    });

    $('input.column_filter').on( 'click', function () {
        filterColumn( 6 );
    });

    $('#min, #max').keyup( function() {
        table.draw();
    } );

    $(document).ready(function($) {
        $("#items").on('click','tr','a',function() {
            window.location = $(this).data("href");
        });
    });
});
