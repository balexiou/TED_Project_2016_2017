/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Beans;

import java.sql.*;
import java.util.ArrayList;

public class SearchBean {
    private int userId;                 //user unique ID
    private String location;            //user location 
    private String street;
    private String neighbourhood;
    private int accomodates;
    private String startDate;
    private String endDate;
    private ArrayList search_rs;



    //  Getters and setters

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
    
    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getNeighbourhood() {
        return neighbourhood;
    }

    public void setNeighbourhood(String neighbourhood) {
        this.neighbourhood = neighbourhood;
    }

    public String getLocation() {
        return location;
    }

    public int getAccomodates() {
        return accomodates;
    }
    
    public ArrayList getSearch_rs(){
        return search_rs;
    }
    
    public void setAccomodates(String accomodates) {
        if("".equals(accomodates)) {
            this.accomodates = -1;
        }else {
            this.accomodates = Integer.parseInt(accomodates);
        }
    }
    
    public void setLocation(String location) {
        this.location = location;
    }
    
    public void setSearch_rs(ArrayList returned_search_rs){
        search_rs = returned_search_rs;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
        
}