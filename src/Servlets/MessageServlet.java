/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Beans.AllMessagesBean;
import Beans.UserBean;
import DAOs.MessageDAO;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


public class MessageServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
            AllMessagesBean message = new AllMessagesBean();
            HttpSession session = request.getSession(true);
            UserBean user =(UserBean) session.getAttribute("Info");
            message.setUsername(user.getUsername());
            System.out.println("Looking for: "+ user.getUsername() );
            message = MessageDAO.returnMessages(message);
            
            
            session.setAttribute("messages", message);
            response.sendRedirect("inbox.jsp");

        } catch (Throwable theException) {
            System.out.println("MessageServlet Failed: " + theException);
        }

    }
}
