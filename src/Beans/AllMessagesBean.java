
package Beans;
import java.util.ArrayList;

public class AllMessagesBean {
    String username;				//username of messager
    ArrayList inMessages;	//array list of received messages 
    ArrayList outMessages;	//array list if sent messages		


	// Getters and Setters
    public String getUsername() {  
        return username;
    }

    public void setUsername(String user_name) {
        username = user_name;
    }

    public ArrayList getIn_messages() {
        return inMessages;
    }

    public void setIn_messages(ArrayList in_mess) {
        inMessages = in_mess;
    }

    public ArrayList getOut_messages() {
        return outMessages;
    }

    public void setOut_messages(ArrayList out_mess) {
        outMessages = out_mess;
    }

}
