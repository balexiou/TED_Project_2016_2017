<%--
  Created by IntelliJ IDEA.
  User: charis
  Date: 9/24/2017
  Time: 12:30 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="Beans.ListingBean" %>
<%@ page import="Beans.UserBean" %>
<%@ page import="DAOs.UserDAO" %>
<%@ page import="DAOs.ListingDAO" %>
<%@ page import="DAOs.BookDAO" %>
<%@ page import="java.util.Objects" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width-device-width, initial-scale1=1">
    <meta name="description" content="Vacation Rentals, Homes, Experiences & Places">
    <meta name="keywords" content="hospitality service, rent, lease, vacation rentals, apartment rentals, homestays, hostel, hostel beds, hotel rooms, experiences, places">
    <meta name="author" content="Babis Alexiou">

    <%
        ListingBean listingBean = (ListingBean) session.getAttribute("Listing");
        UserBean hostBean = new UserBean();
        hostBean.setUserID(listingBean.getHost_id());
        hostBean = UserDAO.displayUserID(hostBean);
        session.setAttribute("message_to", hostBean.getUsername());
        UserBean userBean = (UserBean) session.getAttribute("Info");
        if (userBean != null) {
            int userID = userBean.getUserID();
            if (BookDAO.userNullBookings(userID)){
                ListingDAO.addListingViewed(userID, listingBean.getId());
            }
        }
    %>

    <title><%=listingBean.getName()%> | Host & Chill</title>
    <link rel="shortcut icon"  href="/favicon.ico?" />

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/modal.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="css/admin.css">
    <link rel="stylesheet" href="css/register.css">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css"/>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Oxygen" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">

    <!-- Latest compiled and minified JavaScript -->
    <script src="js/jquery.js"></script>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootsrap.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/user.js"></script>
    <script src="js/validatePassword.js"></script>
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60" onload="mapInit();">
<style>
    #brand_image {
        height: 30px;
    }

    #map {
        height: 400px;
        width: auto;
    }
</style>

<!-- Login Modal starts here -->
<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none">
    <div class="modal-dialog">
        <div class="loginmodal-container">
            <h1 style="font-family: 'Camelia', cursive;">Login to your Account</h1>
            <hr/>
            <form action="LoginServlet/">
                <input type="text" name="username" id="username" placeholder="Username">
                <input type="password" name="password" id="password" placeholder="Enter Your Password">
                <input type="submit" name="login" class="loginmodal-submit" value="Login">
            </form>
            <div class="login-help">
                <a href="register.jsp">Register</a> - <a href="forgotPassword.jsp">Forgot Password</a>
            </div>
        </div>
    </div>
</div>
<!-- End Modal Login -->

<!-- Navigation Bar starts here -->
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <%
                if(userBean == null) {

            %>
            <a class="navbar-brand" href="index.jsp">
                <img id="brand_image" src="images/LOGO.jpg" alt="">
            </a>
            <%
                } else {
            %>
            <a class="navbar-brand" href="logged.jsp">
                <img id="brand_image" src="images/LOGO.jpg" alt="">
            </a>
            <%
                }
            %>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav navbar-right">
                <%
                    if (userBean == null) {
                %>
                <li><a href="register.jsp">Sign Up</a></li>
                <li><a href="#" data-toggle="modal" data-target="#login-modal">Log In</a></li>
                <%
                    } else {
                %>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Welcome, <%= userBean.getFirst_name() %> <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><img src="<%=userBean.getPicture()%>" id="profile_image" class=" img-circle" alt=""> <%= userBean.getFirst_name() %></li>
                        <li class="divider"></li>
                        <li><a href="DisplayUserServlet?username=<%=userBean.getUsername()%>"><i class="fa fa-id-card-o" aria-hidden="true"></i> My Profile</a></li>
                        <li><a href="bookings.jsp"><i class="fa fa-suitcase" aria-hidden="true"></i> My Bookings</a></li>
                        <li><a href="MessageServlet"><i class="fa fa-inbox" aria-hidden="true"></i> Inbox</a></li>
                        <li><a href="sendmessage.jsp"  onclick="window.open('sendmessage.jsp', 'newwindow', 'width=770, height=350'); return false;"><i class="fa fa-envelope" aria-hidden="true"></i> Contact Support</a></li>
                        <li class="divider"></li>
                        <li><a href="LogoutServlet/"><i class="fa fa-sign-out " aria-hidden="true"></i> Logout</a></li>
                    </ul>
                </li>
                <%
                    }
                %>
            </ul>
        </div>
    </div>
</nav>
<!-- Navigation Bar ends here -->

<div class="bg-info" style="background-color: #feefed">
    <div class="container" style="margin-top: 40px">
        <div class="row">
            <h3 align="center" style="font-family: 'Raleway', sans-serif"><%=listingBean.getName()%></h3>
            <hr/>
            <div class="col-md-5">
                <img id="image" src="<%=listingBean.getMedium_url()%>" data-zoom-image="<%=listingBean.getXl_picture_url()%>" class="img-thumbnail" style="width: 400px; height: auto;">
            </div>
            <div class="col-md-5 col-md-offset-1">
                <h3 style="font-family: 'Raleway', sans-serif" align="center">About the listing: </h3>
                <hr/>
                <p align="center"><strong>Room type:</strong> <%=listingBean.getRoom_type()%></p>
                <p align="center"><strong>Number of Beds:</strong> <%=listingBean.getBeds()%></p>
                <p align="center"><strong>Number of Bedrooms:</strong> <%=listingBean.getBedrooms()%></p>
                <p align="center"><strong>Number of Bathrooms:</strong> <%=listingBean.getBathrooms()%></p>
                <p align="center"><strong>Space area:</strong> <%=listingBean.getSquare_feet()%> square feet</p>
            </div>
        </div>
        <div class="row" style="font-family: 'Raleway', sans-serif" align="center">
            <h3 align="left">Description:</h3>
            <p><%=listingBean.getDescription()%></p>
        </div>
        <div class="row">
            <div class="col-md-8" style="font-family: Raleway, sans-serif">
                <h3 align="center">Host Info</h3>
                <div class="col-sm-4">
                    <img src="<%=listingBean.getHost_picture_url()%>" class="img-circle" id="host_picture" style="width: 100%; height: auto">
                </div>
                <div class="col-sm-8" style="font-family: Raleway, sans-serif">
                    <p>
                        <strong>Host Name:</strong> <%=listingBean.getHost_name()%>
                        <i class="fa fa-check-circle" aria-hidden="true" style="color: #3CB371"></i>
                        <%
                            if (userBean != null && !Objects.equals(userBean.getUsername(), listingBean.getHost_name())) {
                        %>
                        <a href="sendmessage.jsp" onclick="window.open('sendmessage.jsp', 'newwindow', 'width=770, height=350'); return false;"><i class="fa fa-envelope" aria-hidden="true"></i>
                        </a>

                        <%
                            }
                        %>
                    </p>
                    <p><strong>Host About:</strong> <%=listingBean.getHost_about()%></p>
                </div>
            </div>

            <div class="col-md-4" style="text-align: center; font-family: Raleway, sans-serif">
                <h3 align="center">Amenities</h3>
                <hr/>
                <%
                    String text = listingBean.getAmenities();
                    String Smoking_Allowed = "Smoking Allowed";
                    String Pets_Allowed = "Pets Allowed";
                    String Events = "Suitable for Events";
                    if (text.contains(Pets_Allowed)) {
                %>
                <p><strong>Pets:</strong> <i class="fa fa-check-circle" aria-hidden="true" style="color: #3CB371"></i></p>
                <%
                    } else {
                %>
                <p><strong>Pets:</strong> <i class="fa fa-times-circle" aria-hidden="true" style="color: #fa4646"></i></p>
                <%
                    }
                    if (text.contains(Smoking_Allowed)) {
                %>
                <p><strong>Smoking:</strong> <i class="fa fa-check-circle" aria-hidden="true" style="color: #3CB371"></i></p>
                <%
                    } else {
                %>
                <p><strong>Smoking:</strong> <i class="fa fa-times-circle" aria-hidden="true" style="color: #fa4646"></i></p>
                <%
                    } if (text.contains(Events)) {
                %>
                <p><strong>Events:</strong> <i class="fa fa-check-circle" aria-hidden="true" style="color: #3CB371"></i></p>
                <%
                    } else {
                %>
                <p><strong>Events:</strong> <i class="fa fa-times-circle" aria-hidden="true" style="color: #fa4646"></i></p>
                <%
                    }
                %>
                <p><strong>Minimum Nights:</strong> <%=listingBean.getMinimum_nights()%></p>
            </div>
        </div>
        <div class="row" style="font-family: Raleway, sans-serif">
            <h3 align="center">Location</h3>
            <hr/>
            <p><strong>Address:</strong> <%=listingBean.getStreet()%></p>
            <p><strong>Neighbourhood:</strong> <%=listingBean.getNeighbourhood()%></p>
            <p><strong>Transit:</strong> <%=listingBean.getTransit()%></p>

            <div id="map" class="container"></div>
        </div>
        <%
            if (userBean != null && userBean.getUserID() != listingBean.getHost_id()){
        %>
        <div class="row" style="font-family: Raleway, sans-serif">
            <h3 align="center">Book now</h3>
            <div class="col-sm-4 col-sm-offset-4">
                <form action="BookServlet" method="post">
                    <center>
                        <button type="submit" class="btn btn-primary btn-md login-button">Book now</button>
                    </center>
                </form>
            </div>
        </div>
        <%
            }
        %>
    </div>
</div>
<script>
    var marker;
    var lat, lng;
    var myLatLng = {lat: <%=listingBean.getLatitude()%>, lng: <%=listingBean.getLongitude()%>};


    function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 15,
            center: myLatLng
        });
        marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            draggable: true
        });
    }
</script>

<script async defer type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDHOVBagaelUjIKq_4IvDT9dDkYnqQvXDY&callback=initMap"></script>

<!-- Plugin JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>
<script src="vendor/scrollreveal/scrollreveal.min.js"></script>
<script src="vendor/magnific-popup/jquery.magnific-popup.min.js"></script>

<!-- Theme JavaScript -->
<script src="js/creative.min.js"></script>

<!-- Elevate Zoom JavaScript -->
<script src="js/jquery.elevateZoom-3.0.8.min.js"></script>
<script>
    $("#image").elevateZoom();
</script>

<!-- Include Date Range Picker -->
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />

<script type="text/javascript" src="js/datepicker.js"></script>

</body>
</html>
