package Servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;

import DAOs.UserDAO;
import Beans.UserBean;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

/**
 * Created by Babis on 5/26/2017.
 */
@WebServlet(name = "RegisterServlet")
public class RegisterServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        boolean isMultipart = ServletFileUpload.isMultipartContent(request);
        boolean ok = false;
        boolean url = false;
        System.out.println("Mpika1");

        if (isMultipart) {
            FileItemFactory factory = new DiskFileItemFactory();
            ServletFileUpload upload = new ServletFileUpload(factory);

            try {

                HttpSession session = request.getSession(true);
                session.setAttribute("Message", null);
                System.out.println("Mpika2");
                UserBean userBean = new UserBean();

                List items = upload.parseRequest(request);
                Iterator iterator = items.iterator();

                while (iterator.hasNext()) {
                    FileItem fileItem = (FileItem) iterator.next();
                    if (!fileItem.isFormField() && !url) {
                        String fileName = fileItem.getName();
                        String root = getServletContext().getRealPath("/");
                        File path = new File(root + "/userimg");
                        if (!path.exists()) {
                            path.mkdirs();
                        }
                        File uploadedFile = new File(path + "/" + fileName);
                        System.out.println(uploadedFile.getAbsolutePath());
                        fileItem.write(uploadedFile);
                        userBean.setPicture("userimg/" + fileName);
                    }
                    else {
                        System.out.println("Mpika3");
                        switch (fileItem.getFieldName()) {
                            case "first_name":
                                System.out.println("First Name: " + fileItem.getString());
                                userBean.setFirst_name(fileItem.getString());
                                break;
                            case "last_name":
                                System.out.println("Last Name:" + fileItem.getString());
                                userBean.setLast_name(fileItem.getString());
                                break;
                            case "email":
                                System.out.println("Email: " + fileItem.getString());
                                userBean.setEmail(fileItem.getString());
                                break;
                            case "PhoneNumber":
                                System.out.println("Phonenumber: " + fileItem.getString());
                                userBean.setPhonenumber(fileItem.getString());
                                break;
                            case "username":
                                System.out.println("Username: " + fileItem.getString());
                                userBean.setUsername(fileItem.getString());
                                break;
                            case "password":
                                System.out.println("Password: " + fileItem.getString());
                                userBean.setPassword(fileItem.getString());
                                break;
                            case "isHost":
                                System.out.println("Host: TRUE");
                                userBean.setHost(true);
                                ok = true;
                                break;
                            case "isRenter":
                                System.out.println("Renter: TRUE");
                                userBean.setRenter(true);
                                ok = true;
                                break;
                            case "PictureURL":
                                if (!Objects.equals(fileItem.getString(), "")) {
                                    System.out.println("Picture URL: " + fileItem.getString());
                                    userBean.setPicture(fileItem.getString());
                                    url = true;
                                }
                                break;
                            default:
                                break;
                        }
                    }
                }

                userBean.setAdmin(false);

                userBean = UserDAO.register(userBean);

                if (userBean.isValid()) {
                    session.setAttribute("current_session_user", userBean);
                    session.setAttribute("Info", userBean);
                    response.sendRedirect("index.jsp");
                }
                else {
                    session.setAttribute("Msg", "Username or email already exists!");
                    response.sendRedirect("ue_error.jsp");
                }

            } catch (Throwable thException){
                System.out.println(thException);
            }
        }
    }

}
