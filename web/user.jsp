<%--
  Created by IntelliJ IDEA.
  User: Babis
  Date: 9/15/2017
  Time: 3:04 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ page import="Beans.UserBean" import="java.util.ArrayList" import="java.util.Objects" %>
<%@ page import="Beans.BookBean" %>
<%@ page import="java.awt.print.Book" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width-device-width, initial-scale1=1">
    <meta name="description" content="Vacation Rentals, Homes, Experiences & Places">
    <meta name="keywords" content="hospitality service, rent, lease, vacation rentals, apartment rentals, homestays, hostel, hostel beds, hotel rooms, experiences, places">
    <meta name="author" content="Babis Alexiou">

    <%
        UserBean info = (UserBean) session.getAttribute("Info");
    %>

    <title> <%=info.getFirst_name()%> Profile  | Host & Chill  </title>
    <link rel="shortcut icon"  href="/favicon.ico?" />


    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="css/user.css">
    <link rel="stylesheet" href="css/register.css">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css"/>
    <!-- Latest compiled and minified JavaScript -->
    <script src="js/jquery.js"></script>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootsrap.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/user.js"></script>
    <script src="js/validatePassword.js"></script>

</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">

<style>
    #brand_image {
        height: 30px;
    }

</style>

<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="logged.jsp">
                <img id="brand_image" src="images/LOGO.jpg" alt="">
            </a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/logged.jsp#about">About</a></li>
                <li><a href="/logged.jsp#becomeahost">Become a host</a></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Welcome, <%= info.getFirst_name() %> <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><img src="<%=info.getPicture()%>" id="profile_image" class=" img-circle" alt=""> <%= info.getFirst_name() %></li>
                        <li class="divider"></li>
                        <li><a href="DisplayUserServlet?username=<%=info.getUsername()%>"><i class="fa fa-id-card-o" aria-hidden="true"></i> My Profile</a></li>
                        <li><a href="bookings.jsp"><i class="fa fa-suitcase" aria-hidden="true"></i> My Bookings</a></li>
                        <li><a href="MessageServlet"><i class="fa fa-inbox" aria-hidden="true"></i> Inbox</a></li>
                        <li><a href="sendmessage.jsp"  onclick="window.open('sendmessage.jsp', 'newwindow', 'width=770, height=350'); return false;"><i class="fa fa-envelope" aria-hidden="true"></i> Contact Support</a></li>
                        <li class="divider"></li>
                        <li><a href="LogoutServlet/"><i class="fa fa-sign-out " aria-hidden="true"></i> Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>

<div id="fullscreen_bg" class="fullscreen_bg"/>
<div class="container">
    <div class="row">
        <div class="col-lg-5 col-md-12 col-sm-8 col-xs-9 bhoechie-tab-container">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 bhoechie-tab-menu">
                <ul class="list-group">
                    <a href="#" class="list-group-item active">
                        <br/><br/><i class="fa fa-user" aria-hidden="true"></i> About<br/><br/>
                    </a>
                    <a href="#" class="list-group-item ">
                        <br/><br/><i class="fa fa-suitcase" aria-hidden="true"></i> Bookings<br/><br/>
                    </a>
                    <%
                        if(info.getIsHost()){

                    %>
                    <a href="#" class="list-group-item ">
                        <br/><br/><i class="fa fa-home" aria-hidden="true"></i> My Listings<br/><br/>
                    </a>
                    <%
                        }
                    %>
                    <a href="#" class="list-group-item ">
                        <br/><br/><i class="fa fa-inbox" aria-hidden="true"></i> Inbox<br/><br/>
                    </a>
                    <a href="#" class="list-group-item">
                        <br/><br/><i class="fa fa-cogs" aria-hidden="true"></i> Settings<br/><br/>
                    </a>
                </ul>
            </div>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab">
                <!-- flight section -->
                <div class="bhoechie-tab-content active">
                    <center>
                        <img src="<%=info.getPicture()%>" id="user_profile_image" class="img-circle img-thumbnail" alt=""> <br>
                        <hr/>
                        <h2 style="margin-top: 5px;color:#00001a">Welcome</h2>
                        <h3 style="margin-top: 0;color:#00001a">
                            <%=info.getFirst_name()%> <%=info.getLast_name()%>
                        </h3>
                        <%
                            if (info.isVerified()) {
                        %>
                        <i class="fa fa-check-circle fa" aria-hidden="true" style="color: #3CB371;"></i>
                        <%
                        }
                        else {
                        %>
                        <p><i class="fa fa-check-circle fa" aria-hidden="true"></i></p>
                        <%
                            }
                        %>
                        <hr/>
                        <h5 align="center">My Information</h5>
                        <hr/>
                        <p align="left">Username: <%=info.getUsername()%></p>
                        <p align="left">E-mail: <%=info.getEmail()%></p>
                        <p align="left">Phonenumber: <%=info.getPhonenumber()%></p>
                    </center>
                </div>


                <div class="bhoechie-tab-content">
                    <center>
                        <i class=" fa fa-suitcase" aria-hidden="true" style="font-size:12em;color:#e17366"></i>
                        <h2 style="margin-top: 0;color:#00001a">My Bookings</h2>
                        <hr/>
                        <a href="bookings.jsp">
                            <button type="submit" class="btn btn-primary btn-md login-button">View My Bookings</button>
                        </a>
                    </center>
                </div>

                <%
                    System.out.println(info.getIsHost());
                    if (info.getIsHost()) {

                %>
                <div class="bhoechie-tab-content">
                    <center>
                        <i class=" fa fa-home" aria-hidden="true" style="font-size:12em;color:#e17366"></i>
                        <h2 style="margin-top: 0;color:#00001a">My Listings</h2>
                        <hr/>
                        <a href="addListing.jsp">
                            <button type="submit" class="btn btn-primary btn-md login-button">Add New Listing</button>
                        </a>
                        <a href="ManageListingServlet">
                            <button type="submit" class="btn btn-primary btn-md login-button">View my Listings</button>
                        </a>
                    </center>
                </div>
                <%
                    }
                %>

                <div class="bhoechie-tab-content">
                    <center>
                        <i class=" fa fa-inbox" aria-hidden="true" style="font-size:12em;color:#e17366"></i>
                        <h2 style="margin-top: 0;color:#00001a">Inbox</h2>
                        <hr/>
                        <a href="MessageServlet">
                            <button type="submit" class="btn btn-primary btn-md login-button">View my Messages</button>
                        </a>
                    </center>
                </div>

                <div class="bhoechie-tab-content">
                    <center>
                        <i class="fa fa-cogs" aria-hidden="true" style="font-size:12em;color:#e17366"></i>
                        <h2 style="margin-top: 0;color:#00001a">Information Settings</h2>
                        <hr/>
                        <a href="updateuser.jsp">
                            <button type="submit" class="btn btn-primary btn-md login-button">Edit My Information</button>
                        </a>
                    </center>
                </div>
            </div>
        </div>
    </div>
</div>

</body>
</html>
