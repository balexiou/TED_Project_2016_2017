package DAOs;

import Beans.BookBean;
import Beans.ListingBean;
import Beans.UserBean;
import DB.ConnectionManager;
import ExportList.ExportList;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import java.io.File;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.List;

/**
 * Created by charis on 9/22/2017.
 */
public class ExportDAO {
    static Connection current_connection = null;
    static ResultSet rs = null;
    static ResultSet rs1 = null;

    public static void exportUsers(String path) {

        UserBean userBean;
        File file = new File(path + "/users.xml");
        Statement stmt = null;
        ExportList<UserBean> exportList = new ExportList<UserBean>();
        String search_query = "SELECT * FROM user";

        System.out.println(file.getAbsolutePath());

        try {
            current_connection = ConnectionManager.getConnection();
            stmt = current_connection.createStatement();
            rs = stmt.executeQuery(search_query);

            while (rs.next()) {

                userBean = new UserBean();

                userBean.setUserID(rs.getInt("userID"));
                userBean.setUsername(rs.getString("username"));
                userBean.setPassword(rs.getString("password"));
                userBean.setEmail(rs.getString("email"));
                userBean.setFirst_name(rs.getString("first_name"));
                userBean.setLast_name(rs.getString("last_name"));
                userBean.setPhonenumber(rs.getString("phonenumber"));
                userBean.setPicture(rs.getString("picture"));
                userBean.setAdmin(rs.getBoolean("isAdmin"));
                userBean.setHost(rs.getBoolean("isHost"));
                userBean.setRenter(rs.getBoolean("isRenter"));
                userBean.setVerified(rs.getBoolean("isVerified"));

                exportList.getValues().add(userBean);
            }
        } catch (Exception ex){
            System.out.println("exportUsers failed: An exception has occurred! " + ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e){
                }
                rs = null;
            }

            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e){
                }
                stmt = null;
            }

            if (current_connection != null) {
                try {
                    current_connection.close();
                } catch (Exception e){
                }
                current_connection = null;
            }
        }

        try {

            JAXBContext jaxbContext = JAXBContext.newInstance(ExportList.class, UserBean.class);
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(exportList, file);

        } catch (Exception ex) {
            System.out.println("exportUsers marshalling failed: " + ex);
        }
    }

    public static void exportListings(String path) {

        ListingBean listing;
        File file = new File(path + "/listings.xml");
        Statement stmt = null;
        ExportList<ListingBean> exportList = new ExportList<ListingBean>();
        String search_query = "SELECT * FROM listings";

        System.out.println(file.getAbsolutePath());

        try {

            current_connection = ConnectionManager.getConnection();
            stmt = current_connection.createStatement();
            rs = stmt.executeQuery(search_query);

            while (rs.next()) {

                listing = new ListingBean();

                listing.setId(rs.getInt("id"));
                listing.setName(rs.getString("name"));
                listing.setSummary(rs.getString("summary"));
                listing.setDescription(rs.getString("description"));
                listing.setExperiences_offered(rs.getString("experiences_offered"));
                listing.setNeighborhood_overview(rs.getString("neighborhood_overview"));
                listing.setNotes(rs.getString("notes"));
                listing.setTransit(rs.getString("transit"));
                listing.setHost_id(rs.getInt("host_id"));
                listing.setHost_name(rs.getString("host_name"));
                listing.setStreet(rs.getString("street"));
                listing.setNeighbourhood(rs.getString("neighbourhood"));
                listing.setCity(rs.getString("city"));
                listing.setState(rs.getString("state"));
                listing.setZipcode(rs.getString("zipcode"));
                listing.setMarket(rs.getString("market"));
                listing.setCountry_code(rs.getString("country_code"));
                listing.setCountry(rs.getString("country"));
                listing.setLatitude(rs.getFloat("latitude"));
                listing.setLongitude(rs.getFloat("longitude"));
                listing.setProperty_type(rs.getString("property_type"));
                listing.setRoom_type(rs.getString("room_type"));
                listing.setAccommodates(rs.getInt("accommodates"));
                listing.setBathrooms(rs.getInt("bathrooms"));
                listing.setBedrooms(rs.getInt("bedrooms"));
                listing.setBeds(rs.getInt("beds"));
                listing.setBed_type(rs.getString("bed_type"));
                listing.setAmenities(rs.getString("amenities"));
                listing.setSquare_feet(rs.getInt("square_feet"));
                listing.setPrice(rs.getFloat("price"));
                listing.setCleaning_fee(rs.getFloat("cleaning_fee"));
                listing.setGuests_included(rs.getInt("guests_included"));
                listing.setMinimum_nights(rs.getInt("minimum_nights"));
                listing.setMaximum_nights(rs.getInt("maximum_nights"));

                exportList.getValues().add(listing);

            }

        } catch (Exception ex) {
            System.out.println("exportListings failed: An exception has occured! " + ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e){
                }
                rs = null;
            }

            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e){
                }
                stmt = null;
            }

            if (current_connection != null) {
                try {
                    current_connection.close();
                } catch (Exception e){
                }
                current_connection = null;
            }
        }

        try {

            JAXBContext jaxbContext = JAXBContext.newInstance(ExportList.class, ListingBean.class);
            Marshaller marshaller = jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(exportList, file);

        } catch (Exception ex) {
            System.out.println("listingsExport marshalling failed! " + ex);
        }
    }

    public static void exportBookings(String path) {

        BookBean booking;
        File file = new File(path + "/bookings.xml");
        Statement stmt = null;
        ExportList<BookBean> exportList = new ExportList<BookBean>();
        String search_query = "SELECT * FROM bookings";

        System.out.println(file.getAbsolutePath());

        try {

            current_connection = ConnectionManager.getConnection();
            stmt = current_connection.createStatement();
            rs = stmt.executeQuery(search_query);

            while (rs.next()) {

                booking = new BookBean();

                booking.setUserId(rs.getInt("user_id"));
                booking.setListingId(rs.getInt("listing_id"));
                booking.setListingThumbNail(rs.getString("listing_url"));
                booking.setStartDate(rs.getString("start_date"));
                booking.setEndDate(rs.getString("end_date"));

                exportList.getValues().add(booking);
            }

        } catch (Exception ex) {
            System.out.println("exportBookings failed: An exception has occurred " + ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e){
                }
                rs = null;
            }

            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e){
                }
                stmt = null;
            }

            if (current_connection != null) {
                try {
                    current_connection.close();
                } catch (Exception e){
                }
                current_connection = null;
            }
        }
        try {

            JAXBContext jaxbContext = JAXBContext.newInstance(ExportList.class, BookBean.class);
            Marshaller marshaller =jaxbContext.createMarshaller();
            marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            marshaller.marshal(exportList, file);

        } catch (Exception ex) {
            System.out.println("exportBookings marshalling failed: " + ex);
        }
    }
}
