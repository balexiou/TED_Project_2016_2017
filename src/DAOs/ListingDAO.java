package DAOs;

import Beans.ListingBean;
import DB.ConnectionManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

/**
 * Created by Babis on 9/14/2017.
 */
public class ListingDAO {

    static Connection current_connection = null;
    static ResultSet rs = null;

    public static ListingBean returnListingObject(ListingBean bean) {

        Statement stmt = null;
        int listingID = bean.getId();

        String search_query = "SELECT * FROM listings WHERE id = '" + listingID +"'";

        try {
            /* Connect to DB */
            current_connection = ConnectionManager.getConnection();
            stmt = current_connection.createStatement();
            rs = stmt.executeQuery(search_query);

            boolean more = rs.next();

            if (more) {
                bean.setId(rs.getInt("id"));
                bean.setName(rs.getString("name"));
                bean.setCity(rs.getString("city"));
                bean.setState(rs.getString("state"));
                bean.setCountry(rs.getString("country"));
                bean.setLatitude(rs.getFloat("latitude"));
                bean.setLongitude(rs.getFloat("longitude"));
                bean.setThumbnail_url(rs.getString("thumbnail_url"));
                bean.setMedium_url(rs.getString("medium_url"));
                bean.setPicture_url(rs.getString("picture_url"));
                bean.setXl_picture_url(rs.getString("xl_picture_url"));
                bean.setHost_since(rs.getDate("host_since"));
                bean.setHost_name(rs.getString("host_name"));
                bean.setHost_name(rs.getString("host_name"));
                bean.setHost_id(rs.getInt("host_id"));
                bean.setDescription(rs.getString("description"));
                bean.setBeds(rs.getInt("beds"));
                bean.setBedrooms(rs.getInt("bedrooms"));
                bean.setBathrooms(rs.getInt("bathrooms"));
                bean.setSquare_feet(rs.getInt("square_feet"));
                bean.setProperty_type(rs.getString("property_type"));
                bean.setRoom_type(rs.getString("room_type"));
                bean.setAmenities(rs.getString("amenities"));
                bean.setMinimum_nights(rs.getInt("minimum_nights"));
                bean.setStreet(rs.getString("street"));
                System.out.println(bean.getStreet());
                bean.setTransit(rs.getString("transit"));
                bean.setNeighbourhood(rs.getString("neighbourhood"));
                bean.setHost_picture_url(rs.getString("host_picture_url"));
                bean.setHost_about(rs.getString("host_about"));
                bean.setPrice(rs.getFloat("price"));
                bean.setExtra_people(rs.getInt("extra_people"));
                bean.setAccommodates(rs.getInt("accommodates"));
            }
            else {
                System.out.println("Unexpected Error occured: Item not found!");
            }

        } catch (Exception ex){
            System.out.println("DisplayListing Failed: An Exception has occurred! " + ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
                rs = null;
            }

            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e) {
                }
                stmt = null;
            }

            if (current_connection != null) {
                try {
                    current_connection.close();
                } catch (Exception e) {
                }

                current_connection = null;
            }
        }

        return bean;
    }

    public static ArrayList getAllListingsFromUser(int userId) {

        Statement stmt = null;
        ArrayList al = null;
        ArrayList search_array = new ArrayList();
        String search_query = "SELECT * FROM listings WHERE host_id = " +userId;
        System.out.println(search_query);

        try {
            /* Connect to DB */
            current_connection = ConnectionManager.getConnection();
            stmt = current_connection.createStatement();
            rs = stmt.executeQuery(search_query);

            while (rs.next()) {
                al = new ArrayList();
                al.add(rs.getString("thumbnail_url"));
                al.add(rs.getString("name"));
                al.add(rs.getInt("accommodates"));
                al.add(rs.getFloat("price"));
                al.add(rs.getString("street"));
                al.add(rs.getInt("id"));

                search_array.add(al);
            }
        } catch (Exception ex){
            System.out.println("Search failed: An Exception has occurred! " + ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
                rs = null;
            }

            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e) {
                }
                stmt = null;
            }

            if (current_connection != null) {
                try {
                    current_connection.close();
                } catch (Exception e) {
                }
                current_connection = null;
            }
        }

        return search_array;
    }

    public static Boolean deleteListing(int listingId) {
        Boolean deleted = false;
        String delete_query;
        String delete_calendar_query;
        String delete_views_query;
        int affected_rows;
        PreparedStatement delete_stmt = null;

        delete_query = "DELETE FROM listings WHERE id = ?";
        delete_calendar_query = "DELETE FROM calendar WHERE listing_id = ?";
        delete_views_query = "DELETE FROM viewedlistings WHERE listing_id = ?";

        try {

            current_connection = ConnectionManager.getConnection();
            delete_stmt = current_connection.prepareStatement(delete_calendar_query);
            delete_stmt.setInt(1, listingId);
            affected_rows = delete_stmt.executeUpdate();

            if (affected_rows != 0) {
                System.out.println("Success. Calendar info for listing deleted!");
            }
            else {
                System.out.println("Something went wrong when deleting calendar info for the listing");
            }

            delete_stmt = current_connection.prepareStatement(delete_views_query);
            delete_stmt.setInt(1, listingId);
            affected_rows = delete_stmt.executeUpdate();

            if (affected_rows != 0) {
                System.out.println("Success. Viewed Listings infro was deleted!");
            }
            else {
                System.out.println("Something went wrong with Viewed Listings deletion");
            }

            delete_stmt = current_connection.prepareStatement(delete_query);
            delete_stmt.setInt(1, listingId);
            affected_rows = delete_stmt.executeUpdate();

            if (affected_rows != 0) {
                System.out.println("Success. Listing deleted!");
            }
            else {
                System.out.println("Something went wrong with deletion of Listing");
            }

        } catch(Exception ex){
            System.out.println("DeleteListing failed: An exception has occurred! " + ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
                rs = null;
            }

            if (delete_stmt != null) {
                try {
                    delete_stmt.close();
                } catch (Exception e) {
                }
                delete_stmt = null;
            }

            if (current_connection != null) {
                try {
                    current_connection.close();
                } catch (Exception e) {
                }
                current_connection = null;
            }
        }

        return  deleted;
    }

    public static ListingBean addListing(ListingBean bean, String dateRange){
        PreparedStatement pstmt = null;
        PreparedStatement pstmt_date = null;

        String[] parts = dateRange.split(" ");
        String startDate = parts[0];
        String endDate = parts[2];

        try {
            current_connection = ConnectionManager.getConnection();

            String insert_query = null;

            insert_query = "INSERT INTO listings (name, street, neighbourhood, room_type, price, extra_people, " +
                    "accommodates, minimum_nights, beds, bedrooms, bathrooms, transit,city, state, country, " +
                    "square_feet, description, host_id, host_name, latitude, longitude, amenities, host_picture_url, " +
                    "thumbnail_url, medium_url)" +
                    "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
            pstmt = current_connection.prepareStatement(insert_query);
            pstmt.setString(1, bean.getName());
            pstmt.setString(2, bean.getStreet());
            pstmt.setString(3, bean.getNeighbourhood());
            pstmt.setString(4, bean.getRoom_type());
            pstmt.setFloat(5, bean.getPrice());
            pstmt.setInt(6, bean.getExtra_people());
            pstmt.setInt(7, bean.getAccommodates());
            pstmt.setInt(8, bean.getMinimum_nights());
            pstmt.setInt(9, bean.getBeds());
            pstmt.setInt(10, bean.getBedrooms());
            pstmt.setInt(11, bean.getBathrooms());
            pstmt.setString(12, bean.getTransit());
            pstmt.setString(13, bean.getCity());
            pstmt.setString(14, bean.getState());
            pstmt.setString(15, bean.getCountry());
            pstmt.setInt(16, bean.getSquare_feet());
            pstmt.setString(17, bean.getDescription());
            pstmt.setInt(18, bean.getHost_id());
            pstmt.setString(19, bean.getHost_name());
            pstmt.setFloat(20, bean.getLatitude());
            pstmt.setFloat(21, bean.getLongitude());
            pstmt.setString(22, bean.getAmenities());
            pstmt.setString(23, bean.getHost_picture_url());
            pstmt.setString(24, bean.getThumbnail_url());
            pstmt.setString(25, bean.getMedium_url());
            pstmt.execute();

            System.out.println("Insertion of new listing into database successful");

            String dates_query = null;
            dates_query
                    = "select a.Date from ("
                    + "    select curdate() + INTERVAL (a.a + (10 * b.a) + (100 * c.a)) DAY as Date"
                    + "    from (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as a"
                    + "    cross join (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as b"
                    + "    cross join (select 0 as a union all select 1 union all select 2 union all select 3 union all select 4 union all select 5 union all select 6 union all select 7 union all select 8 union all select 9) as c"
                    + ") a "
                    //+ "where a.Date between '"+ startDate +"' and '"+ endDate +"' ";
                    + "where a.Date between ? and ?";

            pstmt_date = current_connection.prepareStatement(dates_query);
            pstmt_date.setString(1, startDate);
            pstmt_date.setString(2, endDate);

            System.out.println(pstmt_date);
            rs = pstmt_date.executeQuery();

            insert_query = null;
            Float price = bean.getPrice();
            while (rs.next())
            {
                insert_query
                        = "insert into calendar "
                        + "select MAX(id),'"
                        + rs.getString("Date")
                        + "','t','"+ price
                        + "' from listings";

                pstmt.executeUpdate(insert_query);
                System.out.println(insert_query);
            }

        } catch (Exception ex) {
            System.out.println("addListing failed: An Exception has occurred!" + ex);
        }
        finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e){
                }
                rs = null;
            }

            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception e){
                }
                pstmt = null;
            }

            if (pstmt_date != null) {
                try {
                    pstmt_date.close();
                } catch (Exception e) {
                }
                pstmt_date = null;
            }

            if (current_connection != null) {
                try {
                    current_connection.close();
                } catch (Exception e){
                }
                current_connection = null;
            }
        }

        return bean;

    }

    public static void addListingViewed(int userId, int listingId) {
        PreparedStatement pstmt = null;

        String insert_query = "INSERT INTO viewedlistings(userid, listing_id) VALUES(?, ?)";

        try {
            /* Connect to DB */
            current_connection = ConnectionManager.getConnection();
            pstmt = current_connection.prepareStatement(insert_query);
            pstmt.setInt(1, userId);
            pstmt.setInt(2, listingId);
            System.out.println(insert_query);
            pstmt.executeUpdate();
        } catch (Exception ex) {
            System.out.println("addListingViewed failed: An Exception has occurred!" + ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
                rs = null;
            }

            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception e) {
                }
                pstmt = null;
            }

            if (current_connection != null) {
                try {
                    current_connection.close();
                } catch (Exception e) {
                }
                current_connection = null;
            }
        }
    }

    public static ListingBean updateListing(ListingBean bean, String dateRange) {
        PreparedStatement pstmt = null;
        PreparedStatement pstmt_date = null;
        PreparedStatement pstmt_update = null;

        String insert_query = null;
        int affected_rows;

        String[] parts = dateRange.split(" ");
        String startDate = parts[0];
        String endDate = parts[2];

        String search_query = "SELECT * FROM listings WHERE id = ? FOR UPDATE";

        String update_query = "UPDATE listings SET id = ?, name = ?, street = ?,  neighbourhood = ?, room_type = ?, " +
                "price = ?, extra_people = ?, accommodates = ?, minimum_nights = ?, beds = ?, bedrooms = ?, " +
                "bathrooms = ?, transit = ?, city = ?, state = ?, country = ?, square_feet = ?, description = ?, " +
                "host_id = ?, host_name = ?, latitude = ?, longitude = ?, amenities = ?, host_picture_url = ?, " +
                "thumbnail_url = ?, medium_url = ? WHERE id = ?";

        try {
            current_connection = ConnectionManager.getConnection();
            pstmt = current_connection.prepareStatement(search_query);
            pstmt.setInt(1, bean.getId());

            System.out.println(pstmt);
            rs = pstmt.executeQuery();

            if (rs.next()) {
                pstmt_update = current_connection.prepareStatement(update_query);
                pstmt_update.setInt(1, bean.getId());
                pstmt_update.setString(2, bean.getName());
                pstmt_update.setString(3, bean.getStreet());
                pstmt_update.setString(4, bean.getNeighbourhood());
                pstmt_update.setString(5, bean.getRoom_type());
                pstmt_update.setFloat(6, bean.getPrice());
                pstmt_update.setInt(7, bean.getExtra_people());
                pstmt_update.setInt(8, bean.getAccommodates());
                pstmt_update.setInt(9, bean.getMinimum_nights());
                pstmt_update.setInt(10, bean.getBeds());
                pstmt_update.setInt(11, bean.getBedrooms());
                pstmt_update.setInt(12, bean.getBathrooms());
                pstmt_update.setString(13, bean.getTransit());
                pstmt_update.setString(14, bean.getCity());
                pstmt_update.setString(15, bean.getState());
                pstmt_update.setString(16, bean.getCountry());
                pstmt_update.setInt(17, bean.getSquare_feet());
                pstmt_update.setString(18, bean.getDescription());
                pstmt_update.setInt(19, bean.getHost_id());
                pstmt_update.setString(20, bean.getHost_name());
                pstmt_update.setFloat(21, bean.getLatitude());
                pstmt_update.setFloat(22, bean.getLongitude());
                pstmt_update.setString(23, bean.getAmenities());
                pstmt_update.setString(24, bean.getHost_picture_url());
                pstmt_update.setString(25, bean.getThumbnail_url());
                pstmt_update.setString(26, bean.getMedium_url());
                pstmt_update.setInt(27, bean.getId());

                affected_rows = pstmt_update.executeUpdate();
                System.out.println(pstmt_update);
                System.out.println(affected_rows);

                System.out.println("Update of listings: Successful!");
                String dates_query = null;

                dates_query
                        = "SELECT a.Date FROM ("
                        + "    SELECT curdate() + INTERVAL (a.a + (10 * b.a) + (100 * c.a)) DAY as Date"
                        + "    FROM (SELECT 0 as a UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 "
                        + "    UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 "
                        + "    UNION ALL SELECT 8 UNION ALL SELECT 9) as a"
                        + "    CROSS JOIN (SELECT 0 as a UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 "
                        + "    UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 "
                        + "    UNION ALL SELECT 8 UNION ALL SELECT 9) as b"
                        + "    CROSS JOIN (SELECT 0 as a UNION ALL SELECT 1 UNION ALL SELECT 2 UNION ALL SELECT 3 "
                        + "    UNION ALL SELECT 4 UNION ALL SELECT 5 UNION ALL SELECT 6 UNION ALL SELECT 7 "
                        + "    UNION ALL SELECT 8 UNION ALL SELECT 9 as c"
                        + ") a "
                        //+ "where a.Date between '"+ startDate +"' and '"+ endDate +"' ";
                        + "WHERE a.Date BETWEEN ? AND ?";

                pstmt_date = current_connection.prepareStatement(dates_query);
                pstmt_date.setString(1, startDate);
                pstmt_date.setString(2, endDate);

                System.out.println(pstmt_date);

                rs = pstmt_date.executeQuery();

                Float price = bean.getPrice();
                while (rs.next()) {
                    insert_query = "INSERTO INTO calendar "+ " " + bean.getId() + ",'" + rs.getString("Date")
                            +"', 't','" + price +"' FROM listings";

                    pstmt.executeUpdate(insert_query);
                    System.out.println(insert_query);
                }
            }else {
                System.out.println("Update Query failed!");
            }
        } catch (Exception ex){
            System.out.println("UpdateListing failed: An Exception has occurred!" + ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
                rs = null;
            }

            if (pstmt != null) {
                try {
                    pstmt.close();
                } catch (Exception e) {
                }
                pstmt = null;
            }

            if (pstmt_update != null) {
                try {
                    pstmt_update.close();
                } catch (Exception e) {
                }
                pstmt_update = null;
            }

            if (pstmt_date != null) {
                try {
                    pstmt_date.close();
                } catch (Exception e){
                }
                pstmt_date = null;
            }

            if (current_connection != null) {
                try {
                    current_connection.close();
                } catch (Exception e) {
                }
                current_connection = null;
            }
        }
        return bean;
    }
}
