/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Servlets;

import Beans.MessageBean;
import Beans.UserBean;
import DAOs.MessageDAO;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class MessageSeenServlet extends HttpServlet {
    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, java.io.IOException {

        UserBean user = new UserBean();
        HttpSession session = request.getSession(true);
        user = (UserBean) session.getAttribute("Info");
        String username = user.getUsername();
        
        int message_id;
        int counter;
        message_id = Integer.parseInt(request.getParameter("messageID"));

        MessageBean message = new MessageBean();
        
        message = MessageDAO.seen(message_id);
        counter = MessageDAO.UnreadMessages(username);
        
        session.setAttribute("message", message);
        session.setAttribute("unread", counter);
        
        response.sendRedirect("readmessage.jsp");

    }
}
