/**
 * Created by Babis on 9/1/2017.
 */

$(function () {

    var $formLogin = $('#login-form');
    var $formLost = $('#forgot-form');
    var $formRegister = $('#register-form');
    var $divForms = $('#div-forms');
    var $modalAnimateTime = 300;
    var $msgAnimateTime = 150;
    var $msgShowTime = 2000;

    $("form").submit(function () {
        switch (this.id) {
            case "login-form":
                var $lg_username=$('#login_username').val();
                var $lg_password=$('#login_password').val();
                if ($lg_username == "ERROR") {
                    msgChange($('#div-login-msg'), $('#icon-login-msg'), $('#text-login-msg'),
                        "error", "glyphicon-remove", "Login Error");
                } else {
                    msgChange($('#div-login-msg'), $('#icon-login-msg'), $('#text-login-msg'),
                        "success", "glyphicon-ok", "Login Success");
                }
                console.log("MPIKA 1");
                break;
            case "forgot-form":
                var $ls_email=$('#lost_email').val();
                if ($ls_email == "ERROR") {
                    msgChange($('#div-lost-msg'), $('#icon-lost-msg'), $('#text-lost-msg'),
                        "error", "glyphicon-remove", "Send Error");
                } else {
                    msgChange($('#div-lost-msg'), $('#icon-lost-msg'), $('#text-lost-msg'),
                        "success", "glyphicon-ok", "Send Success");
                }
                console.log("MPIKA 2");
                break;
            case "register-form":
                var $rg_firstname=$('#register_first_name').val();
                var $rg_lastname=$('#register_last_name').val();
                var $rg_email=$('#register_email').val();
                var $rg_phonenumber=$('#register_phonenumber').val();
                var $rg_username=$('#register_username').val();
                var $rg_password=$('#register_password').val();
                var $register_role_host=$('#register_role_host').val();
                var $register_role_renter=$('#register_role_renter').val();
                var $register_picture_url=$('#register_picture_url').val();
                var $register_picture_file=$('#register_picture_file').val();
                if ($rg_username == "ERROR") {
                    msgChange($('#div-register-msg'), $('#icon-register-msg'), $('#text-register-msg'),
                        "error", "glyphicon-remove", "Register Error");
                } else {
                    msgChange($('#div-register-msg'), $('#icon-register-msg'), $('#text-register-msg'),
                        "success", "glyphicon-ok", "Register Success");
                }
                console.log("MPIKA 3");
                break;
            default:
                console.log("GAMITHIKE TO SYMPAN");
                return false;
        }
    });

    $('#login_register_btn').click(function () { modalAnimate ($formLogin, $formRegister);});
    $('#register_login_btn').click(function () { modalAnimate($formRegister, $formLogin);});
    $('#login_lost_btn').click( function () { modalAnimate($formLogin, $formLost); });
    $('#lost_login_btn').click( function () { modalAnimate($formLost, $formLogin); });
    $('#lost_register_btn').click( function () { modalAnimate($formLost, $formRegister); });
    $('#register_lost_btn').click( function () { modalAnimate($formRegister, $formLost); });

    function modalAnimate($oldForm, $newForm) {
        var $oldH = $oldForm.height();
        var $newH = $newForm.height();
        $divForms.css("height", $oldH);
        $oldForm.fadeToggle($modalAnimateTime, function () {
            $divForms.animate({height:  $newH}, $modalAnimateTime, function () {
                $newForm.fadeToggle($modalAnimateTime);
            });
        });
    }

    function msgFade($msgId, $msgText) {
        $msgId.fadeOut($msgAnimateTime, function () {
            $(this).text($msgText).fadeIn($msgAnimateTime);
        });
    }

    function msgChange($divTag, $iconTag, $textTag, $divClass, $iconClass, $msgText) {
        var $msgOld = $divTag.text();
        msgFade($textTag, $msgText);
        $divTag.addClass($divClass);
        $iconTag.removeClass("glyphicon-chevron-right");
        $iconTag.addClass($iconClass + " " + $divClass);
        setTimeout(function() {
            msgFade($textTag, $msgOld);
            $divTag.removeClass($divClass);
            $iconTag.addClass("glyphicon-chevron-right");
            $iconTag.removeClass($iconClass + " " + $divClass);
        }, $msgShowTime);
    }

});
