package Servlets;

import DAOs.ListingDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by charis on 9/28/2017.
 */
@WebServlet(name = "DeleteListingServlet")
public class DeleteListingServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int listing_id;
        Boolean isDeleted;

        listing_id = Integer.parseInt(request.getParameter("listingId"));
        isDeleted = ListingDAO.deleteListing(listing_id);

        response.sendRedirect("user.jsp");
    }
}
