<%--
  Created by IntelliJ IDEA.
  User: charis
  Date: 9/28/2017
  Time: 8:09 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="Beans.UserBean" %>
<%@ page import="Beans.BookBean" %>
<%@ page import="java.util.ArrayList" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width-device-width, initial-scale1=1">
    <meta name="description" content="Vacation Rentals, Homes, Experiences & Places">
    <meta name="keywords" content="hospitality service, rent, lease, vacation rentals, apartment rentals, homestays, hostel, hostel beds, hotel rooms, experiences, places">
    <meta name="author" content="Babis Alexiou">

    <title>My Bookings | Host & Chill</title>
    <link rel="shortcut icon"  href="/favicon.ico?" />

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="css/admin.css">
    <link rel="stylesheet" href="css/register.css">
    <link rel="stylesheet" href="css/addListing.css">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css"/>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Oxygen" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">

    <!-- Latest compiled and minified JavaScript -->
    <script src="js/jquery.js"></script>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootsrap.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/user.js"></script>
    <script src="js/validatePassword.js"></script>
    <script src="js/datepicker.js"></script>
    <script src="js/navigation.js"></script>
    <script src="js/back_to_top.js"></script>

    <%
        UserBean info = (UserBean) session.getAttribute("Info");
        BookBean bookBean = (BookBean) session.getAttribute("Bookings");
        ArrayList rs = bookBean.getSearch_rs();
    %>
</head>
<body>
<%
    if (info != null) {
%>
<div class="container">
    <div class="row main">
        <div class="panel-heading">
            <div class="panel-title text-center">
                <h1 class="title"> Host & Chill </h1>
                <hr/>
                <h2 class="title">Your Bookings</h2>
            </div>
        </div>

        <%
            if (!rs.isEmpty()) {
        %>
        <table id="bookings" class="table table-bordered" style="table-layout: auto; font-family: Raleway, sans-serif">
            <thead>
            <tr>
                <th></th>
                <th style="text-align: center; font-family: Raleway, sans-serif; color: #fff;" >First day</th>
                <th style="text-align: center; font-family: Raleway, sans-serif; color: #fff;">Last day</th>
            </tr>
            </thead>
            <%
                for (int i = 0; i < rs.size(); i++) {
                    ArrayList rs2 = (ArrayList) rs.get(i);

            %>
            <tr class="clickable-row" data-href="DisplayListingServlet?id=<%=rs2.get(1)%>">
                <th><a href="DisplayListingServlet?id=<%=rs2.get(1)%>"><img src="<%=rs2.get(2)%>" class="img-thumbnail" style="object-fit: contain; width: 185px; height: 120px"></a></th>
                <th style="text-align: center"><%=rs2.get(3)%></th>
                <th style="text-align: center"><%=rs2.get(4)%></th>
            </tr>
            <%
                }
            %>
        </table>
        <%
            } else {
        %>
        <h5 align="center" style="font-family: Raleway, sans-serif; color: #fff;">You have made no bookings yet! :(</h5>
        <%
            }
        %>
        <a href="user.jsp"><h5 align="center" style="font-family: Raleway, sans-serif;">Back</h5></a>
    </div>
</div>
<%
    } else {
%>
<!-- Login or Register Message -->
<div class="container">
    <div class="row main">
        <div class="panel-heading">
            <div class="panel-title text-center">
                <h1 class="title"> Host & Chill </h1>
                <hr/>
                <h2 class="title">Register or Login!</h2>
            </div>
        </div>
    </div>
</div>
<%
    }
%>


<!-- DataTables JavaScript -->
<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.15/datatables.min.css"/>
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.15/datatables.min.js"></script>

<script>
    $(document).ready(function() {
        $('#bookings').DataTable( {
            "columnDefs": [
                {
                    "orderable": false,
                    "targets": [0]
                }],
            "dom": '<"top"i>rt<"bottom"lp><"clear">'
        } );
    } );
</script>
</body>
</html>
