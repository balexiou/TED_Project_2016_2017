package Servlets;

import Beans.ListingBean;
import Beans.UserBean;
import DAOs.ListingDAO;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by charis on 9/21/2017.
 */
@WebServlet(name = "AddListingServlet")
public class AddListingServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        boolean isMultipart = ServletFileUpload.isMultipartContent(request);

        if (isMultipart) {
            FileItemFactory factory = new DiskFileItemFactory();
            ServletFileUpload upload = new ServletFileUpload(factory);

            try {
                List<String> valueList = new ArrayList<String>();
                String date_range = null;

                ListingBean listingBean = new ListingBean();
                UserBean userBean = null;
                HttpSession session = request.getSession(true);
                userBean = (UserBean) session.getAttribute("Info");
                System.out.println("UserID: " + userBean.getUserID());
                System.out.println("First Name: " + userBean.getFirst_name());
                System.out.println("Picture: " + userBean.getPicture());

                listingBean.setHost_id(userBean.getUserID());
                listingBean.setHost_name(userBean.getFirst_name());
                listingBean.setHost_picture_url(userBean.getPicture());

                List items = upload.parseRequest(request);
                Iterator iterator = items.iterator();

                while (iterator.hasNext()) {
                    FileItem fileItem = (FileItem) iterator.next();
                    if (!fileItem.isFormField()) {
                        String fileName = fileItem.getName();
                        String root = getServletContext().getRealPath("/");
                        File path = new File(root + "/listingimg");
                        if (!path.exists()) {
                            path.mkdirs();
                        }
                        File uploadedFile = new File(path + "/" + fileName);
                        System.out.println(uploadedFile.getAbsolutePath());
                        fileItem.write(uploadedFile);
                        listingBean.setThumbnail_url("listingimg/" + fileName);
                        listingBean.setMedium_url("listingimg/" + fileName);
                    }
                    else {
                        switch (fileItem.getFieldName()) {
                            case "name":
                                System.out.println("Name: " + fileItem.getString());
                                listingBean.setName(fileItem.getString());
                                break;
                            case "street":
                                System.out.println("Street: " + fileItem.getString());
                                listingBean.setStreet(fileItem.getString());
                                break;
                            case "neighbourhood":
                                System.out.println("Neighbourhood: " + fileItem.getString());
                                listingBean.setNeighbourhood(fileItem.getString());
                                break;
                            case "room_type":
                                System.out.println("Room Type: " + fileItem.getString());
                                listingBean.setRoom_type(fileItem.getString());
                                break;
                            case "price":
                                System.out.println("Price" + fileItem.getString());
                                listingBean.setPrice(Float.parseFloat(fileItem.getString()));
                                break;
                            case "extra_cost":
                                System.out.println("Extra Cost: " + fileItem.getString());
                                listingBean.setExtra_people(Integer.parseInt(fileItem.getString()));
                                break;
                            case "accommodates":
                                System.out.println("Accommodates: " + fileItem.getString());
                                listingBean.setAccommodates(Integer.parseInt(fileItem.getString()));
                                break;
                            case "minimum_nights":
                                System.out.println("minimum_nights: " + fileItem.getString());
                                listingBean.setMinimum_nights(Integer.parseInt(fileItem.getString()));
                                break;
                            case "beds":
                                System.out.println("beds: " + fileItem.getString());
                                listingBean.setBeds(Integer.parseInt(fileItem.getString()));
                                break;
                            case "bedrooms":
                                System.out.println("bedrooms: " + fileItem.getString());
                                listingBean.setBedrooms(Integer.parseInt(fileItem.getString()));
                                break;
                            case "bathrooms":
                                System.out.println("bathrooms: " + fileItem.getString());
                                listingBean.setBathrooms(Integer.parseInt(fileItem.getString()));
                                break;
                            case "square_feet":
                                System.out.println("square_feet: " + fileItem.getString());
                                listingBean.setSquare_feet(Integer.parseInt(fileItem.getString()));
                                break;
                            case "transit":
                                System.out.println("transit: " + fileItem.getString());
                                listingBean.setTransit(fileItem.getString());
                                break;
                            case "city":
                                System.out.println("city: " + fileItem.getString());
                                listingBean.setCity(fileItem.getString());
                                break;
                            case "state":
                                System.out.println("state: " + fileItem.getString());
                                listingBean.setState(fileItem.getString());
                                break;
                            case "country":
                                System.out.println("country: " + fileItem.getString());
                                listingBean.setCountry(fileItem.getString());
                                break;
                            case "description":
                                System.out.println("description: " + fileItem.getString());
                                listingBean.setDescription(fileItem.getString());
                                break;
                            case "lat":
                                System.out.println("lat: " + fileItem.getString());
                                listingBean.setLatitude(Float.parseFloat(fileItem.getString()));
                                break;
                            case "lng":
                                System.out.println("lng: " + fileItem.getString());
                                listingBean.setLongitude(Float.parseFloat(fileItem.getString()));
                                break;
                            case "amenities":
                                System.out.println("amenities: " + fileItem.getString());
                                valueList.add(fileItem.getString());
                                break;
                            case "available":
                                System.out.println("available: " + fileItem.getString());
                                date_range = fileItem.getString();
                                break;

                            default:
                                break;
                        }
                    }
                }

                String string = String.join(",", valueList);
                System.out.println(string);
                listingBean.setAmenities(string);
                ListingDAO.addListing(listingBean, date_range);

                response.sendRedirect("ManageListingServlet");

            } catch (Throwable thException) {
                System.out.println("Servlet Failed!:" + thException);
            }
        }

    }
}
