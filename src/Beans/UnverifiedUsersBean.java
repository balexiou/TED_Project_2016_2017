package Beans;

/**
 * Created by Babis on 9/13/2017.
 */

import java.util.ArrayList;

public class UnverifiedUsersBean {

    private ArrayList<String> UnverifiedUsers_rs;

    public ArrayList<String> getUnverifiedUsers_rs() {
        return UnverifiedUsers_rs;
    }

    public void setUnverifiedUsers_rs(ArrayList<String> unverified_users) {
        UnverifiedUsers_rs = unverified_users;
    }

}
