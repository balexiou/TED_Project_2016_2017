package DAOs;

/**
 * Created by Babis on 5/25/2017.
 */

import Beans.BookBean;
import Beans.UserBean;
import DB.ConnectionManager;
import java.text.*;
import java.util.*;
import java.sql.*;

public class UserDAO {

    static Connection current_connection = null;
    static ResultSet rs = null;

    public static UserBean login(UserBean bean) {

        // Preparing the needed objects for the log in action

        Statement stmt = null;

        String username = bean.getUsername();
        String password = bean.getPassword();

        String search_query = "SELECT * FROM user WHERE username='"
                + username
                + "' AND password='"
                +password
                +"'";

        System.out.println("Username: " + username);
        System.out.println("Password: " + password);
        System.out.println("Query: " + search_query);

        try {
            // Connect to the Database
            current_connection = ConnectionManager.getConnection();
            stmt = current_connection.createStatement();
            rs = stmt.executeQuery(search_query);
            boolean more = rs.next();

            // If user does not exist set the IsValid variable to false
            if (!more) {
                System.out.println("Sorry, but it seems like you are not registered, or your credentials are wrong! " +
                        "Please try again or register first.");
                bean.setValid(false);
            }
            // If user exists set the isValid variable to true
            else if (more) {
                String first_name = rs.getString("first_name");
                String last_name = rs.getString("last_name");

                System.out.println("Welcome, " + first_name);
                bean.setUserID(rs.getInt("userID"));
                bean.setPassword("unavailable");
                bean.setFirst_name(rs.getString("first_name"));
                bean.setLast_name(rs.getString("last_name"));
                bean.setEmail(rs.getString("email"));
                bean.setPhonenumber(rs.getString("phonenumber"));
                bean.setPicture(rs.getString("picture"));
                bean.setHost(rs.getBoolean("isHost"));
                bean.setVerified(rs.getBoolean("verified"));
                bean.setAdmin(rs.getBoolean("isAdmin"));
                bean.setValid(true);
            }
        } catch (Exception e) {
            System.out.println("Log in failed: An exception has ocurred! " + e);
        }
        // Exception handling
        finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
                rs = null;
            }

            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e) {
                }
                stmt = null;
            }

            if (current_connection != null) {
                try {
                    current_connection.close();
                } catch (Exception e) {
                }
                current_connection = null;
            }
        }

        return bean;

    }

    public static UserBean register(UserBean bean) {

        // Preparing the needed objects for the register action

        Statement stmt = null;

        String username = bean.getUsername();
        String email = bean.getEmail();

        String search_query = "SELECT * FROM user WHERE username='"
                + username
                + "' OR email='"
                + email
                + "'";

        System.out.println("Checking availability: Username = " + username);
        System.out.println("Checking availability: Email = " + email);
        System.out.println("Query: " + search_query);

        try {
            // Connect to the DB
            current_connection = ConnectionManager.getConnection();
            stmt = current_connection.createStatement();
            rs = stmt.executeQuery(search_query);
            boolean more = rs.next();

            // If username and email are available
            if (!more) {
                boolean isAdmin = false;
                System.out.println("Username and/or email are available");
                String insert_query = "INSERT INTO user (username, password, first_name, last_name, email, phonenumber, " +
                        "picture, isAdmin, isHost, isRenter, verified) " +
                        "VALUES (?, ?, ?, ?, ?, ?, ?, ? ,?, ?, ?)";
                PreparedStatement pstmt = current_connection.prepareStatement(insert_query);
                pstmt.setString(1, bean.getUsername());
                pstmt.setString(2, bean.getPassword());
                pstmt.setString(3, bean.getFirst_name());
                pstmt.setString(4, bean.getLast_name());
                pstmt.setString(5, bean.getEmail());
                pstmt.setString(6, bean.getPhonenumber());
                pstmt.setString(7, bean.getPicture());
                pstmt.setBoolean(8,isAdmin);
                pstmt.setBoolean(9, bean.getIsHost());
                pstmt.setBoolean(10, bean.getIsRenter());
                pstmt.setInt(11, 0);
                pstmt.execute();
                //stmt.executeUpdate(insert_query);
                System.out.println("Insertion of new user into database successful");
                bean.setValid(true);
            }
            else if (more) {
                System.out.println("Username or email are not available");
                bean.setValid(false);
            }
        } catch (Exception e) {
            System.out.println("Register failed: An exception has occured! " + e);
        }
        // Exception handling
        finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
                rs = null;
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e) {
                }
                stmt = null;
            }
            if (current_connection != null) {
                try {
                    current_connection.close();
                } catch (Exception e) {
                }
                current_connection = null;
            }
        }

        return bean;

    }

    public static UserBean displayUser(UserBean bean) {

        Statement stmt = null;
        String username = bean.getUsername();

        String search_query = "SELECT * FROM user WHERE username='" + username + "'";

        try {
            current_connection = ConnectionManager.getConnection();
            stmt = current_connection.createStatement();
            rs = stmt.executeQuery(search_query);

            if (rs.next()) {
                bean.setPassword("unavailable");
                bean.setUserID(rs.getInt("userID"));
                bean.setFirst_name(rs.getString("first_name"));
                bean.setLast_name(rs.getString("last_name"));
                bean.setEmail(rs.getString("email"));
                bean.setPhonenumber(rs.getString("phonenumber"));
                bean.setPicture(rs.getString("picture"));
                bean.setHost(rs.getBoolean("isHost"));
                bean.setVerified(rs.getBoolean("verified"));
                bean.setAdmin(rs.getBoolean("isAdmin"));
            }
            else {
                System.out.println("Error! User not found!");
            }
        } catch (Exception e) {
            System.out.println("Display User failed: An exception has occured! " + e);
        }
        finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
                rs = null;
            }
            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e) {
                }
                stmt = null;
            }
            if (current_connection != null) {
                try {
                    current_connection.close();
                } catch (Exception e) {
                }
                current_connection = null;
            }
        }

        return bean;
    }

    public static UserBean updateUser(UserBean bean) {

        Statement stmt = null;
        PreparedStatement update_stmt = null;
        int affected_rows;

        String username = bean.getUsername();
        String email = bean.getEmail();
        String phonenumber = bean.getPhonenumber();

        String search_query = "SELECT * FROM user WHERE (phonenumber='"
                + phonenumber
                + "' OR email='"
                + email
                + "') AND userID !="
                + bean.getUserID();

        System.out.println("Searching availability for username: " + username);
        System.out.println("Searching availability for email:" + email);
        System.out.println("Query: " + search_query);

        try {
            /* Connect to DB */
            current_connection = ConnectionManager.getConnection();
            stmt = current_connection.createStatement();
            rs = stmt.executeQuery(search_query);
            boolean results = rs.next();

            /* If username and email are available */
            if (!results) {
                System.out.println("Username and email are available");

                String update_query = "UPDATE user SET username = ?, password = ?, first_name = ?," +
                        "last_name = ?, email = ?, phonenumber = ?, picture = ?, isHost = ? WHERE userID = ?";

                update_stmt = current_connection.prepareStatement(update_query);
                update_stmt.setString(1, bean.getUsername());
                update_stmt.setString(2, bean.getPassword());
                update_stmt.setString(3, bean.getFirst_name());
                update_stmt.setString(4, bean.getLast_name());
                update_stmt.setString(5, bean.getEmail());
                update_stmt.setString(6, bean.getPhonenumber());
                update_stmt.setString(7, bean.getPicture());
                update_stmt.setBoolean(8, bean.getIsHost());
                update_stmt.setInt(9, bean.getUserID());

                affected_rows = update_stmt.executeUpdate();
                System.out.println(update_stmt);
                System.out.println(affected_rows);
                System.out.println("Update of user: " + bean.getUserID() + " successful!");
                bean.setValid(true);
            }
            else if (results) {
                System.out.println("Username or email are not available");
                bean.setValid(false);
            }
        } catch (Exception ex){
            System.out.println("updateUser failed: An unexpected error occurred. "+ ex);
        }
        finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
                rs = null;
            }

            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e){
                }
                stmt = null;
            }

            if (update_stmt != null) {
                try {
                    update_stmt.close();
                } catch (Exception e) {
                }
                update_stmt = null;
            }

            if (current_connection != null) {
                try {
                    current_connection.close();
                } catch (Exception e) {
                }
                current_connection = null;
            }
        }

        return bean;
    }

    public static BookBean userBookings(BookBean bookBean, UserBean userBean) {

        ArrayList arrayList = null;
        ArrayList searchList = new ArrayList();
        Statement stmt = null;
        int userID = userBean.getUserID();
        String search_query = "SELECT * FROM bookings WHERE user_id = " + userID;
        System.out.println(search_query);

        try {

            current_connection = ConnectionManager.getConnection();
            stmt = current_connection.createStatement();
            rs = stmt.executeQuery(search_query);

            while (rs.next()) {
                arrayList = new ArrayList();
                arrayList.add(rs.getInt("user_id"));
                arrayList.add(rs.getInt("listing_id"));
                arrayList.add(rs.getString("listing_url"));
                arrayList.add(rs.getDate("start_date"));
                arrayList.add(rs.getDate("end_date"));

                searchList.add(arrayList);
            }
            bookBean.setSearch_rs(searchList);

        } catch (Exception ex) {
            System.out.println("userBookings has failed: An exception occurred "+ ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
                rs = null;
            }

            if (stmt != null) {
                try{
                    stmt.close();
                } catch (Exception e){
                }
                stmt = null;
            }

            if (current_connection != null) {
                try {
                    current_connection.close();
                } catch (Exception e) {
                }
                current_connection = null;
            }
        }

        return bookBean;
    }

    public static UserBean displayUserID(UserBean userBean) {

        Statement stmt = null;
        String userID = String.valueOf(userBean.getUserID());

        String search_query = "SELECT * FROM user WHERE userID = '" + userID + "' ";
        try {
            current_connection = ConnectionManager.getConnection();
            stmt = current_connection.createStatement();
            rs = stmt.executeQuery(search_query);

            if (rs.next()) {
                userBean.setPassword("not displayed");
                userBean.setUsername(rs.getString("username"));
                userBean.setFirst_name(rs.getString("first_name"));
                userBean.setLast_name(rs.getString("last_name"));
                userBean.setEmail(rs.getString("email"));
                userBean.setPhonenumber(rs.getString("phonenumber"));
                userBean.setPicture(rs.getString("picture"));
                userBean.setVerified(rs.getBoolean("verified"));

            } else {
                System.out.println("unexpected error user not found");
            }

        } catch (Exception ex) {
            System.out.println("DisplayUserID failed: An Exception has occurred! " + ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
                rs = null;
            }

            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e) {
                }
                stmt = null;
            }

            if (current_connection != null) {
                try {
                    current_connection.close();
                } catch (Exception e) {
                }

                current_connection = null;
            }
        }

        return userBean;
    }

}
