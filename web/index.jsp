<%--
  Created by IntelliJ IDEA.
  User: charis
  Date: 5/25/2017
  Time: 5:23 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8"%>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta name="viewport" content="width-device-width, initial-scale1=1">
  <meta name="description" content="Vacation Rentals, Homes, Experiences & Places">
  <meta name="keywords" content="hospitality service, rent, lease, vacation rentals, apartment rentals, homestays, hostel, hostel beds, hotel rooms, experiences, places">
  <meta name="author" content="Babis Alexiou">

  <title>Host & Chill | Welcome</title>
  <link rel="shortcut icon"  href="/favicon.ico?" />



  <!-- Latest compiled and minified CSS -->
  <link rel="stylesheet" href="css/bootstrap.css">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/footer.css">
  <link rel="stylesheet" href="css/modal.css">
  <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css"/>
  <!-- Latest compiled and minified JavaScript -->
  <script src="js/jquery.js"></script>
  <script src="js/jquery.min.js"></script>
  <script src="js/bootsrap.js"></script>
  <script src="js/bootstrap.min.js"></script>

  <!-- Include Required Prerequisites -->

  <!-- Include Date Range Picker -->
  <script src="daterangepicker/moment.min.js"></script>
  <script src="daterangepicker/daterangepicker.js"></script>
  <link rel="stylesheet" type="text/css" href="daterangepicker/daterangepicker.css" />
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">

<style>
  #brand_image {
    height: 30px;
  }
</style>

<!-- Navigation Bar starts here -->
<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#myPage">
        <img id="brand_image" src="images/LOGO.jpg" alt="">
      </a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="#about">About</a></li>
        <li><a href="#becomeahost">Become a host</a></li>
        <li><a href="register.jsp">Sign Up</a></li>
        <li><a href="#" data-toggle="modal" data-target="#login-modal">Log In</a></li>
      </ul>
    </div>
  </div>
</nav>
<!-- Navigation Bar ends here -->

<!-- Login Modal starts here -->
<div class="modal fade" id="login-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none">
  <div class="modal-dialog">
    <div class="loginmodal-container">
      <h1 style="font-family: 'Camelia' cursive;">Login to your Account</h1>
      <hr/>
      <form action="LoginServlet/">
        <input type="text" name="username" id="username" placeholder="Username">
        <input type="password" name="password" id="password" placeholder="Enter Your Password">
        <input type="submit" name="login" class="loginmodal-submit" value="Login">
      </form>
      <div class="login-help">
        <a href="register.jsp">Register</a> - <a href="forgotPassword.jsp">Forgot Password</a>
      </div>
    </div>
  </div>
</div>
<!-- End Modal Login -->

<!-- Main Search Form starts here -->
<header>
  <div class="jumbotron text-center">
    <h1>Host & Chill</h1>
    <p>Vacation Rentals, Homes, Experiences & Places</p>
    <form action="SearchServlet" class="form-inline" method="post">
      <input type="search" name="location" class="form-control input-lg" placeholder="Destination, City, Address" required>
      <div class="input-group">
        <input type="search" class="form-control input-lg" placeholder="When" name="daterange" value="" required/>
        <div class="input-group-addon">
          <span class="glyphicon glyphicon-calendar"></span>
        </div>
      </div>
      <input style="width: 20%; height: 46px" type="number" min="1" name="accomodates" class="form-control" placeholder="Guests" required>
      <button type="submit" class="btn btn-danger btn-lg">Search</button>
    </form>
  </div>
</header>
<!-- Main Seaarch Form ends here -->


<!-- Recommended Houses starts here -->
<!-- <div class="wrapper-white padded">
  <div class="container">
    <h2 class="text-center">Your next trip</h2>
    <div class="row">
      <div class="col-xs-12 col-sm-4">
        <div class="card" style="background: linear-gradient(rgba(0,0,0,0.3), rgba(0,0,0,0.2)),
                    url('images/card-paris.jpg');">
          <div class="card-category">Paris</div>
          <div class="card-description">
            <h2>Seb's home</h2>
            <p>Charming room with patio</p>
          </div>
          <img class="card-user avatar avatar-large" src="images/seb.jpg">
          <a class="card-link" href="#"></a>
        </div>
      </div>
      <div class="col-xs-12 col-sm-4">
        <div class="card" style="background: linear-gradient(rgba(0,0,0,0.3), rgba(0,0,0,0.2)),
                    url('images/card-brussels.jpeg');">
          <div class="card-category">Brussels</div>
          <div class="card-description">
            <h2>Anne's home</h2>
            <p>Lovely house</p>
          </div>
          <img class="card-user avatar avatar-large" src="images/anne.jpg">
          <a class="card-link" href="#"></a>
        </div>
      </div>
      <div class="col-xs-12 col-sm-4">
        <div class="card" style="background: linear-gradient(rgba(0,0,0,0.3), rgba(0,0,0,0.2)),
                    url('images/card-london.jpeg');">
          <div class="card-category">London</div>
          <div class="card-description">
            <h2>Romain's home</h2>
            <p>Nice flat in Picadilly</p>
          </div>
          <img class="card-user avatar avatar-large" src="images/romain.jpeg">
          <a class="card-link" href="#"></a>
        </div>
      </div>
    </div>
  </div>
</div> -->

<!-- Recommended Houses ends here -->

<!-- Destinations Carousel starts here -->
<div class="container-fluid">
  <h2 class="text-center">Other Destinations</h2>
  <br>
  <div id="myCarousel" class="carousel slide" data-ride="carousel" data-interval="3000">
    <!-- Indicators -->
    <ol class="carousel-indicators">
      <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
      <li data-target="#myCarousel" data-slide-to="1"></li>
      <li data-target="#myCarousel" data-slide-to="2"></li>
      <li data-target="#myCarousel" data-slide-to="3"></li>
      <li data-target="#myCarousel" data-slide-to="4"></li>
      <li data-target="#myCarousel" data-slide-to="5"></li>
      <li data-target="#myCarousel" data-slide-to="6"></li>
      <li data-target="#myCarousel" data-slide-to="7"></li>
      <li data-target="#myCarousel" data-slide-to="8"></li>
      <li data-target="#myCarousel" data-slide-to="9"></li>
      <li data-target="#myCarousel" data-slide-to="10"></li>
      <li data-target="#myCarousel" data-slide-to="11"></li>
      <li data-target="#myCarousel" data-slide-to="12"></li>
      <li data-target="#myCarousel" data-slide-to="13"></li>
      <li data-target="#myCarousel" data-slide-to="14"></li>
      <li data-target="#myCarousel" data-slide-to="15"></li>
      <li data-target="#myCarousel" data-slide-to="16"></li>
      <li data-target="#myCarousel" data-slide-to="17"></li>
      <li data-target="#myCarousel" data-slide-to="18"></li>
      <li data-target="#myCarousel" data-slide-to="19"></li>
      <li data-target="#myCarousel" data-slide-to="20"></li>
      <li data-target="#myCarousel" data-slide-to="21"></li>
      <li data-target="#myCarousel" data-slide-to="22"></li>
      <li data-target="#myCarousel" data-slide-to="23"></li>
      <li data-target="#myCarousel" data-slide-to="24"></li>
      <li data-target="#myCarousel" data-slide-to="25"></li>
      <li data-target="#myCarousel" data-slide-to="26"></li>
      <li data-target="#myCarousel" data-slide-to="27"></li>
      <li data-target="#myCarousel" data-slide-to="28"></li>
      <li data-target="#myCarousel" data-slide-to="29"></li>
      <li data-target="#myCarousel" data-slide-to="30"></li>
      <li data-target="#myCarousel" data-slide-to="31"></li>
      <li data-target="#myCarousel" data-slide-to="32"></li>
      <li data-target="#myCarousel" data-slide-to="33"></li>
    </ol>

    <!-- Wrapper for slides -->
    <div class="carousel-inner" role="listbox">
      <div class="item active">
        <img src="images/sanfransico.jpg" alt="San Fransisco" width="375" height="500">
      </div>

      <div class="item">
        <img src="images/budapest.png" alt="Budapest" width="375" height="500">
      </div>

      <div class="item">
        <img src="images/Corfu.jpg" alt="Corfu" width="375" height="500">
      </div>

      <div class="item">
        <img src="images/alaska2.jpg" alt="Alaska" width="375" height="500">
      </div>

      <div class="item">
        <img src="images/bruges.png" alt="Bruges" width="375" height="500">
      </div>

      <div class="item">
        <img src="images/cape_cod.jpg" alt="Cape Cod" width="375" height="500">
      </div>

      <div class="item">
        <img src="images/capri.png" alt="Capri" width="375" height="500">
      </div>

      <div class="item">
        <img src="images/hawaii.jpg" alt="Hawaii" width="375" height="500">
      </div>

      <div class="item">
        <img src="images/miami.png" alt="Miami Beach" width="375" height="500">
      </div>

      <div class="item">
        <img src="images/malibu.png" alt="Malibu" width="375" height="500">
      </div>

      <div class="item">
        <img src="images/miami.jpg" alt="Miami" width="375" height="500">
      </div>

      <div class="item">
        <img src="images/Havana.jpg" alt="Havana" width="375" height="500">
      </div>

      <div class="item">
        <img src="images/sydney.jpg" alt="Sydney" width="375" height="500">
      </div>

      <div class="item">
        <img src="images/mexico.jpg" alt="Mexico" width="375" height="500">
      </div>

      <div class="item">
        <img src="images/sydney2.jpg" alt="Miami" width="375" height="500">
      </div>

      <div class="item">
        <img src="images/mauritius-dodo.png" alt="Mauritius" width="375" height="500">
      </div>

      <div class="item">
        <img src="images/alaska-stellers-sea-cow.png" alt="Alaska" width="375" height="500">
      </div>

      <div class="item">
        <img src="images/costa-rica-golden-toad.png" alt="Costa Rica" width="375" height="500">
      </div>

      <div class="item">
        <img src="images/dubai.png" alt="Dubai" width="375" height="500">
      </div>

      <div class="item">
        <img src="images/iceland.jpg" alt="Iceland" width="375" height="500">
      </div>

      <div class="item">
        <img src="images/india.jpg" alt="India" width="375" height="500">
      </div>

      <div class="item">
        <img src="images/istanbul.jpg" alt="Istanbul" width="375" height="500">
      </div>

      <div class="item">
        <img src="images/moscow.jpg" alt="Moscow" width="375" height="500">
      </div>

      <div class="item">
        <img src="images/new-zealand-moa.png" alt="New Zealand" width="375" height="500">
      </div>

      <div class="item">
        <img src="images/tasmania-thylacine.png" alt="Tasmania" width="375" height="500">
      </div>

      <div class="item">
        <img src="images/EtsPalms.jpg" alt="Palm Springs" width="375" height="500">
      </div>

      <div class="item">
        <img src="images/london.jpg" alt="London" width="375" height="500">
      </div>

      <div class="item">
        <img src="images/tokyo.jpg" alt="Tokyo" width="375" height="500">
      </div>

      <div class="item">
        <img src="images/japan.jpg" alt="Japan" width="375" height="500">
      </div>

      <div class="item">
        <img src="images/paris.jpg" alt="Paris" width="375" height="500">
      </div>

      <div class="item">
        <img src="images/vienna.jpg" alt="Vienna" width="375" height="500">
      </div>

      <div class="item">
        <img src="images/vietnam.jpg" alt="Vietnam" width="375" height="500">
      </div>

      <div class="item">
        <img src="images/moscow2.png" alt="Moscow" width="375" height="500">
      </div>
    </div>

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>
<!-- Destination Carousel ends here -->


<!-- About Section starts here -->
<div class="wrapper-grey padded">
  <div id="about" class="container-fluid">
    <div class="row">
      <div class="col-sm-8">
        <h2>About Company Page</h2><br>
        <h4>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</h4><br>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p>
      </div>
    </div>
  </div>
</div>
<!-- About Section ends here -->

<!-- Become a Host section starts here -->
<div id="becomeahost" class="container-fluid">
  <div class="text-center">
    <h2>Become a host</h2>
    <h4>Fund your passions as a host.<br>Welcome people to your community and help them belong anywhere.</h4>
  </div>
  <div class="row slideanim">
    <div class="col-sm-4 col-xs-12">
      <div class="panel panel-default text-center">
        <div class="panel-heading">
          <h1>Host your extra space</h1>
        </div>
        <div class="panel-body">
          <p>Whether you have a cabin in the mountains or an extra room, earn more income welcoming guests.</p>

        </div>
        <div class="panel-footer">
          <a href="#" data-toggle="modal" data-target="#login-modal">
            <button class="btn btn-lg">List your space</button>
          </a>
        </div>
      </div>
    </div>
    <div class="col-sm-4 col-xs-12">
      <div class="panel panel-default text-center">
        <div class="panel-heading">
          <h1>Host for your neighborhood</h1>
        </div>
        <div class="panel-body">
          <p>Don’t have any spare space to host? You can earn money hosting for others as a neighborhood co-host.</p>
        </div>
        <div class="panel-footer">
          <a href="#" data-toggle="modal" data-target="#login-modal">
            <button class="btn btn-lg">Become a co-host</button>
          </a>
        </div>
      </div>
    </div>
    <div class="col-sm-4 col-xs-12">
      <div class="panel panel-default text-center">
        <div class="panel-heading">
          <h1>Host unique experiences</h1>
        </div>
        <div class="panel-body">
          <p>Share your passion, expertise, or what’s special about where you live leading experiences for travelers.</p>
        </div>
        <div class="panel-footer">
          <a href="#" data-toggle="modal" data-target="#login-modal">
            <button class="btn btn-lg">Create an experience</button>
          </a>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="wrapper-grey padded">
  <div class="container-fluid">
    <h2>Why you’ll love becoming a host</h2>

    <div class="col-sm-4 col-xs-12">
      <h4>Extra income</h4>
      <p>From saving for home repairs to taking a dream trip, use the extra income to fund your passions.</p>
    </div>

    <div class="col-sm-4 col-xs-12">
      <h4>Support</h4>
      <p>Get tips and tools and connect with hosts like you from around the world.</p>
    </div>

    <div class="col-sm-4 col-xs-12">
      <h4>Flexibility</h4>
      <p>You set your price and decide when you want to host and how often.</p>
    </div>
  </div>
</div>

<div class="container text-center padded features">
  <div class="row slideanim">
    <div class="col-xs-12 col-sm-6 col-md-3">
      <img src="images/globe.png" alt="">
      <h2>Discover the world</h2>
      <p>The earth is your new home.</p>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-3">
      <img src="images/dollar.png" alt="">
      <h2>Pay online</h2>
      <p>No cash issues anymore.</p>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-3">
      <img src="images/beer.png" alt="">
      <h2>Meet people</h2>
      <p>Discover new culture and lifestyle.</p>
    </div>
    <div class="col-xs-12 col-sm-6 col-md-3">
      <img src="images/heart.png" alt="">
      <h2>Share and love</h2>
      <p>Join a community you will love.</p>
    </div>
  </div>
</div>
<!-- Become a Host section ends here -->

<!-- Map starts here -->
<div id="map" style="width: 100%;height: 500px;"></div>
<!-- Map ends here -->

<!-- Footer starts here -->
<div id="footer">
  <div class="container">
    <div class="row slideanim">
      <div class="col-xs-4">
        <ul class="list-inline">
          <li><a href=""><i class="fa fa-youtube"></i></a></li>
          <li><a href=""><i class="fa fa-instagram"></i></a></li>
          <li><a href=""><i class="fa fa-facebook"></i></a></li>
          <li><a href=""><i class="fa fa-twitter"></i></a></li>
        </ul>
      </div>
      <div class="col-xs-8 text-right">
        <p>Copyright &copy; 2017 by Host & Chill</p>
      </div>
    </div>
  </div>
</div>
<a id="back-to-top" href="#" class="btn btn-danger btn-lg back-to-top" role="button"
   title="Click to return on the top page" data-toggle="tooltip" data-placement="left">
  <span class="glyphicon glyphicon-chevron-up"></span>
</a>
<!-- Footer ends here -->

<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDHOVBagaelUjIKq_4IvDT9dDkYnqQvXDY&callback=initMap"></script>

<script type="text/javascript">
    var myLatlng = new google.maps.LatLng(37.9866375,23.7348804);

    var myOptions = {
        zoom: 16,
        center: myLatlng,
        scrollwheel: false,
        mapTypeId: google.maps.MapTypeId.ROADMAP,
        styles: [{"featureType":"landscape","stylers":[{"saturation":-100},{"lightness":65},{"visibility":"on"}]},{"featureType":"poi","stylers":[{"saturation":-100},{"lightness":51},{"visibility":"simplified"}]},{"featureType":"road.highway","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"road.arterial","stylers":[{"saturation":-100},{"lightness":30},{"visibility":"on"}]},{"featureType":"road.local","stylers":[{"saturation":-100},{"lightness":40},{"visibility":"on"}]},{"featureType":"transit","stylers":[{"saturation":-100},{"visibility":"simplified"}]},{"featureType":"administrative.province","stylers":[{"visibility":"off"}]},{"featureType":"water","elementType":"labels","stylers":[{"visibility":"on"},{"lightness":-25},{"saturation":-100}]},{"featureType":"water","elementType":"geometry","stylers":[{"hue":"#ffff00"},{"lightness":-25},{"saturation":-97}]}]
    };

    var map = new google.maps.Map(document.getElementById('map'), myOptions);
    var marker = new google.maps.Marker({
        position: myLatlng,
        map: map,
        title:"You are here!"
    });
</script>



<script src="js/datepicker.js"></script>
<script src="js/navigation.js"></script>
<script src="js/back_to_top.js"></script>
</body>
</html>
