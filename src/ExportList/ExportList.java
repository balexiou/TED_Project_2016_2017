package ExportList;

/**
 * Created by charis on 9/22/2017.
 */

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.*;

@XmlRootElement
public class ExportList<VALUE> {
    private List<VALUE> values = new ArrayList<VALUE>();

    @XmlAnyElement(lax = true)
    public List<VALUE> getValues(){
        return  values;
    }
}
