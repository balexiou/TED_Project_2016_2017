package Beans;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * Created by Babis on 5/25/2017.
 */
@XmlRootElement(name="user")
public class UserBean {

    private String username;
    private String password;
    private String first_name;
    private String last_name;
    private String email;
    private String phonenumber;
    private String picture;
    private int userID;
    private boolean isAdmin;
    private boolean isHost;
    private boolean isRenter;
    private boolean verified;
    private boolean valid;

    public boolean isValid() {
        return valid;
    }

    public String getUsername(){
        return username;
    }

    @XmlElement
    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    @XmlElement
    public void setPassword(String password) {
        this.password = password;
    }

    public String getFirst_name() {
        return first_name;
    }

    @XmlElement
    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    @XmlElement
    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    @XmlElement
    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhonenumber() {
        return phonenumber;
    }

    @XmlElement
    public void setPhonenumber(String phonenumber) {
        this.phonenumber = phonenumber;
    }

    public String getPicture() {
        return picture;
    }

    @XmlElement
    public void setPicture(String picture) {
        this.picture = picture;
    }

    public int getUserID() {
        return userID;
    }

    @XmlAttribute
    public void setUserID(int userID){
        this.userID = userID;
    }

    public boolean getIsAdmin() {
        return isAdmin;
    }

    @XmlElement
    public void setAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    public boolean getIsHost() {
        return isHost;
    }

    @XmlElement
    public void setHost(boolean isHost) {
        this.isHost = isHost;
    }

    public boolean getIsRenter() {
        return isRenter;
    }

    @XmlElement
    public void setRenter(boolean isRenter) {
        this.isRenter = isRenter;
    }

    public boolean isVerified() {
        return verified;
    }

    @XmlElement
    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    @XmlElement
    public void setValid(boolean valid) {
        this.valid = valid;
    }

}
