<%--
  Created by IntelliJ IDEA.
  User: Babis
  Date: 9/14/2017
  Time: 1:27 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width-device-width, initial-scale1=1">
    <meta name="description" content="Vacation Rentals, Homes, Experiences & Places">
    <meta name="keywords" content="hospitality service, rent, lease, vacation rentals, apartment rentals, homestays, hostel, hostel beds, hotel rooms, experiences, places">
    <meta name="author" content="Babis Alexiou">

    <title>Host & Chill | Login Error</title>
    <link rel="shortcut icon"  href="/favicon.ico?" />

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/error.css">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css"/>
    <!-- Latest compiled and minified JavaScript -->
    <script src="js/jquery.js"></script>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootsrap.js"></script>
    <script src="js/bootstrap.min.js"></script>

    <!-- Include Required Prerequisites -->

    <!-- Include Date Range Picker -->
    <script src="daterangepicker/moment.min.js"></script>
    <script src="daterangepicker/daterangepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="daterangepicker/daterangepicker.css" />

    <!-- web font -->
    <link href="//fonts.googleapis.com/css?family=Josefin+Sans" rel="stylesheet">
    <link href='//fonts.googleapis.com/css?family=Roboto+Condensed:400,300,300italic,400italic,700,700italic' rel='stylesheet' type='text/css'>
    <!-- //web font -->

</head>
<body>

<!-- conten t-->
<h1>Log In Error!</h1>
<div class="main-wthree">
    <h2>Sorry...</h2>
    <p><span class="sub-agileinfo">But </span>It seems that you entered the wrong username or password. <br>Maybe you are not even registered...</p>
    <br>
    <p><span class="sub-agileinfo">Try again </span>using the correct credentials or register!</p>

    <a href="index.jsp">
        <p><span>Take me back</span></p>
    </a>
</div>

</body>
</html>
