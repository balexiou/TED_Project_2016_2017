<%--
  Created by IntelliJ IDEA.
  User: charis
  Date: 9/28/2017
  Time: 1:30 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="Beans.UserBean" %>
<%@ page import="java.util.ArrayList" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width-device-width, initial-scale1=1">
    <meta name="description" content="Vacation Rentals, Homes, Experiences & Places">
    <meta name="keywords" content="hospitality service, rent, lease, vacation rentals, apartment rentals, homestays, hostel, hostel beds, hotel rooms, experiences, places">
    <meta name="author" content="Babis Alexiou">

    <title>Manage Listings | Host & Chill</title>
    <link rel="shortcut icon"  href="/favicon.ico?" />

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="css/admin.css">
    <link rel="stylesheet" href="css/register.css">
    <link rel="stylesheet" href="css/addListing.css">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css"/>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Oxygen" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">

    <!-- Latest compiled and minified JavaScript -->
    <script src="js/jquery.js"></script>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootsrap.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/user.js"></script>
    <script src="js/validatePassword.js"></script>
    <script src="js/datepicker.js"></script>
    <script src="js/navigation.js"></script>
    <script src="js/back_to_top.js"></script>

    <%
        UserBean info = (UserBean) session.getAttribute("Info");
    %>
</head>
<body>

<%
    if (info.getIsHost() && info != null) {
%>
<!-- Display of user's listings starts here -->
<div class="container">
    <div class="row main">
        <div class="panel-heading">
            <div class="panel-title text-center">
                <h1 class="title"> Host & Chill </h1>
                <hr/>
                <h2 class="title">Your Listings</h2>
            </div>
        </div>
        <%
            if (!info.isVerified()) {
        %>
        <div class="panel-heading">
            <div class="panel-title text-center">
                <h1 class="title"> Host & Chill </h1>
                <hr/>
                <h2 class="title">Verification Pending</h2>
            </div>
        </div>
        <%
            } else {
                ArrayList results = (ArrayList) session.getAttribute("userListings");
                if (results != null && !results.isEmpty()){
        %>
        <table id="listings" class="table table-bordered" style="table-layout: auto">
            <thead>
            <tr>
                <th></th>
                <th>Title</th>
                <th>Guests</th>
                <th>Price</th>
                <th>Location</th>
            </tr>
            </thead>
            <%
                for (int i = 0; i < results.size(); i++) {
                    ArrayList listings = (ArrayList) results.get(i);
                
            %>
            <tr>
                <th><a href="DisplayListingServlet?id=<%=listings.get(5)%>">
                    <img src="<%=listings.get(0)%>" class="img-thumbnail" style="object-fit: contain; width: 160px; height: 120px;">
                </a></th>
                <th><a href="DisplayListingServlet?id=<%=listings.get(5)%>"><%=listings.get(1)%></a></th>
                <th><%=listings.get(2)%></th>
                <th><%=listings.get(3)%>$</th>
                <th><%=listings.get(4)%></th>
                <th>
                    <a href="DeleteListingServlet?listingId=<%=listings.get(5)%>" class="btn btn-danger" role="button">Delete Listing</a>
                    <a href="EditListingServlet?listingId=<%=listings.get(5)%>" class="btn btn-warning" role="button">Edit Listing</a>
                </th>
            </tr>
        </table>
        <%
                    }
                } else {
        %>
        <h5 align="center">You have no Listings!</h5>
        <%
                }
            }
        %>
    </div>
</div>

<%
    } if (!info.getIsHost()) {
%>
<!-- Unauthorized Access Message -->
<div class="container">
    <div class="row main">
        <div class="panel-heading">
            <div class="panel-title text-center">
                <h1 class="title"> Host & Chill </h1>
                <hr/>
                <h2 class="title">Unauthorized Access</h2>
            </div>
        </div>
    </div>
</div>
<%
    } if (info == null) {
%>
<!-- Login or Register Message -->
<div class="container">
    <div class="row main">
        <div class="panel-heading">
            <div class="panel-title text-center">
                <h1 class="title"> Host & Chill </h1>
                <hr/>
                <h2 class="title">Register or Login!</h2>
            </div>
        </div>
    </div>
</div>
<%
    }
%>
</body>
</html>
