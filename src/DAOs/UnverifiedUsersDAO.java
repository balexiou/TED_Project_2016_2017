package DAOs;

/**
 * Created by Babis on 9/13/2017.
 */

import Beans.UnverifiedUsersBean;
import DB.ConnectionManager;
import java.sql.*;
import java.util.ArrayList;

public class UnverifiedUsersDAO {

   static Connection current_connection = null;
   static ResultSet rs = null;

   public static UnverifiedUsersBean unverifiedUsers(UnverifiedUsersBean bean) {

       PreparedStatement stmt = null;
       ArrayList<String> unverified_users_array = new ArrayList<String>();

       String search_query;
       search_query = "SELECT * FROM user WHERE verified = 0";

       try {
           current_connection = ConnectionManager.getConnection();
           stmt = current_connection.prepareStatement(search_query);
           rs = stmt.executeQuery(search_query);

           while (rs.next()) {
               unverified_users_array.add(rs.getString("username"));
           }

           bean.setUnverifiedUsers_rs(unverified_users_array);

       } catch (Exception ex) {
           System.out.println("SELECT unverified failed: An exception has occured!" + ex);
       } finally {
           if (rs != null) {
               try {
                   rs.close();
               } catch (Exception e){
               }
               rs = null;
           }

           if (stmt != null) {
               try {
                   stmt.close();
               } catch (Exception e) {
               }
               stmt = null;
           }

           if (current_connection != null) {
               try {
                   current_connection.close();
               }catch (Exception e) {
               }
               current_connection = null;
           }
       }
       return bean;
   }

   public static void verify_users(UnverifiedUsersBean bean) {

       PreparedStatement stmt = null;
       ArrayList al = null;
       ArrayList<String> users_to_verify = new ArrayList<String>();
       String update_query;
       String user_to_update;
       int affected_rows;

       users_to_verify = bean.getUnverifiedUsers_rs();

       if(!users_to_verify.isEmpty()) {

           try {
               current_connection = ConnectionManager.getConnection();
               for (int i = 0; i < users_to_verify.size(); i++) {
                   affected_rows = 0;
                   stmt = null;
                   update_query = null;
                   user_to_update = users_to_verify.get(i);

                   update_query = "UPDATE user SET verified = 1 WHERE username = ? AND verified = 0";
                   stmt = current_connection.prepareStatement(update_query);
                   stmt.setString(1, user_to_update);
                   affected_rows = stmt.executeUpdate();

                   if (affected_rows == 1) {
                       System.out.println(user_to_update + "was successfully verified!");
                   }
                   else {
                       System.out.println("Error: user verification failed!");
                   }
               }
           } catch (Exception ex) {
               System.out.println("Update user failed: An exception has occured!");
           } finally {
               if (rs != null) {
                   try {
                       rs.close();
                   } catch (Exception e){
                   }
                   rs = null;
               }

               if (stmt != null) {
                   try {
                       stmt.close();
                   } catch (Exception e) {
                   }
                   stmt = null;
               }

               if (current_connection != null) {
                   try {
                       current_connection.close();
                   } catch (Exception e) {
                   }
                   current_connection = null;
               }
           }

       }

       else {
           System.out.println("Unexpected Error: No users to verify!");
       }

   }

}
