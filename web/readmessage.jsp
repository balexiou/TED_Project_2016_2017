<%--
  Created by IntelliJ IDEA.
  User: charis
  Date: 9/29/2017
  Time: 12:31 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="Beans.UserBean" %>
<%@ page import="Beans.MessageBean" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width-device-width, initial-scale1=1">
    <meta name="description" content="Vacation Rentals, Homes, Experiences & Places">
    <meta name="keywords" content="hospitality service, rent, lease, vacation rentals, apartment rentals, homestays, hostel, hostel beds, hotel rooms, experiences, places">
    <meta name="author" content="Babis Alexiou">

    <%
        UserBean info = (UserBean) session.getAttribute("Info");
        MessageBean messageBean = (MessageBean) session.getAttribute("message");
        session.setAttribute("message_to", messageBean.getFrom_user());
    %>

    <title>Message from <%=messageBean.getFrom_user()%> | Host & Chill</title>
    <link rel="shortcut icon"  href="/favicon.ico?" />

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="css/admin.css">
    <link rel="stylesheet" href="css/register.css">
    <link rel="stylesheet" href="css/addListing.css">
    <link rel="stylesheet" href="css/inbox.css">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css"/>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Oxygen" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">

    <!-- Latest compiled and minified JavaScript -->
    <script src="js/jquery.js"></script>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootsrap.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/user.js"></script>
    <script src="js/validatePassword.js"></script>
    <script src="js/datepicker.js"></script>
    <script src="js/navigation.js"></script>
    <script src="js/back_to_top.js"></script>
    <script src="js/datasort.js"></script>
    <script src="js/inbox.js"></script>
</head>
<body>
<%
    if (info != null) {
%>
<div class="container" align="center" style="font-family: Raleway, sans-serif">
    <div class="row main">
        <div class="panel-heading">
            <div class="panel-title text-center">
                <h1 class="title"> Host & Chill </h1>
                <hr/>
                <h2 class="title">Incoming Message</h2>
            </div>
        </div>
        <div class="col-md-10 col-md-offset-1">
            <p align="left" style="color: #fff;">From: <%=messageBean.getFrom_user()%> <a href="sendmessage.jsp"  onclick="window.open('sendmessage.jsp', 'newwindow', 'width=770, height=350'); return false;"><i class="fa fa-envelope" aria-hidden="true"></i></a></p>
            <p align="left" style="color: #fff;"><%=messageBean.getDate()%></p>
            <p align="left" style="color: #fff;"> Message:</p>
            <p align="left" style="color: #fff"><%=messageBean.getMessage()%></p>
            <a href="MessageServlet?user=<%=info.getUsername()%>"><i class="fa fa-chevron-left" aria-hidden="true"></i> Back</a>
        </div>
    </div>
</div>


<%
    } else {
%>
<!-- Login or Register Message -->
<div class="container">
    <div class="row main">
        <div class="panel-heading">
            <div class="panel-title text-center">
                <h1 class="title"> Host & Chill </h1>
                <hr/>
                <h2 class="title">Register or Login!</h2>
            </div>
        </div>
    </div>
</div>
<%
    }
%>

</body>
</html>
