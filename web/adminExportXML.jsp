<%--
  Created by IntelliJ IDEA.
  User: charis
  Date: 9/22/2017
  Time: 4:33 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="Beans.UserBean" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width-device-width, initial-scale1=1">
    <meta name="description" content="Vacation Rentals, Homes, Experiences & Places">
    <meta name="keywords" content="hospitality service, rent, lease, vacation rentals, apartment rentals, homestays, hostel, hostel beds, hotel rooms, experiences, places">
    <meta name="author" content="Babis Alexiou">

    <%
        UserBean info = (UserBean) session.getAttribute("Info");
        //BookBean bookings = (BookBean) session.getAttribute("Bookings");
        //ArrayList results = bookings.getSearch_rs();
        session.setAttribute("Msg", null);
    %>

    <title>Host & Chill | Export XML</title>
    <link rel="shortcut icon"  href="/favicon.ico?" />


    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="css/admin.css">
    <link rel="stylesheet" href="css/register.css">
    <link rel="stylesheet" href="css/panel.css">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css"/>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Oxygen" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">

    <!-- Latest compiled and minified JavaScript -->
    <script src="js/jquery.js"></script>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootsrap.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/user.js"></script>
    <script src="js/validatePassword.js"></script>
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">

<%
    if (info.getIsAdmin()){
%>

<style>
    #brand_image {
        height: 30px;
    }

</style>
<!-- Navbat Starts Here -->
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="admin.jsp">
                <img id="brand_image" src="images/LOGO.jpg" alt="">
            </a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Welcome, <%= info.getFirst_name() %> <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><img src="<%=info.getPicture()%>" id="profile_image" class=" img-circle" alt=""> <%= info.getFirst_name() %></li>
                        <li class="divider"></li>
                        <li><a href="admin.jsp"><i class="fa fa-id-card-o" aria-hidden="true"></i> Admin Panel</a></li>
                        <li><a href="UnverifiedUsersServlet"><i class="fa fa-users" aria-hidden="true"></i> Verify Users</a></li>
                        <li><a href="adminExportXML.jsp"><i class="fa fa-download" aria-hidden="true"></i> Export XML</a></li>
                        <li class="divider"></li>
                        <li><a href="LogoutServlet/"><i class="fa fa-sign-out " aria-hidden="true"></i> Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
<!-- Navbar ends Here! -->

<div class="container text-center" style="margin-top: 40px">
    <h3 style="font-family: 'Raleway', sans-serif; color: #fff">XML Export</h3>
    <hr/>
</div>

<div class="container">
    <div class="row">
        <!-- Bookings Export Panel -->
        <div class="col-sm-4">
            <div class="panel" id="panel_1">
                <div class="panel-heading">Export Bookings XML</div>
                <div class="panel-body">
                    <img src="http://www.itensityonline.com/Images/bookings-icon.png" class="img-responsive" style="width: 100%" alt="Image">
                </div>
                <div class="panel-footer">
                    <form method="post" action="ExportXMLServlet?btn=1">
                        <center>
                        <button class="btn btn-primary btn-md login-button"><i class="fa fa-download" aria-hidden="true"></i> Download Bookings</button>
                        </center>
                    </form>
                </div>
            </div>
        </div>
        <!-- Listins Export Panel -->
        <div class="col-sm-4">
            <div class="panel" id="panel_2">
                <div class="panel-heading">Export Listing XML</div>
                <div class="panel-body">
                    <img src="https://pbs.twimg.com/profile_images/731190991030341632/mtyY6l-G.jpg" class="img-responsive" style="width: 100%" alt="Image">
                </div>
                <div class="panel-footer">
                    <form method="post" action="ExportXMLServlet?btn=2">
                        <center>
                            <button class="btn btn-primary btn-md login-button"><i class="fa fa-download" aria-hidden="true"></i> Download Listings</button>
                        </center>
                    </form>
                </div>
            </div>
        </div>
        <!-- Users Export Panel -->
        <div class="col-sm-4">
            <div class="panel" id="panel_3">
                <div class="panel-heading">Export Users XML</div>
                <div class="panel-body">
                    <img src="https://cdn1.iconfinder.com/data/icons/freeline/32/account_friend_human_man_member_person_profile_user_users-256.png" class="img-responsive" style="width: 100%" alt="Image">
                </div>
                <div class="panel-footer">
                    <form method="post" action="ExportXMLServlet?btn=3">
                        <center>
                            <button class="btn btn-primary btn-md login-button"><i class="fa fa-download" aria-hidden="true"></i> Download Users</button>
                        </center>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<%
    } else {
%>
<h1 align="center" style="font-family: 'Raleway', sans-serif; color: #fff ">Unauthorized Access</h1>
<%
    }
%>
</body>
</html>
