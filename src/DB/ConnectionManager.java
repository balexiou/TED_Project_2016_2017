package DB;

/**
 * Created by Babis on 5/25/2017.
 */

import java.sql.*;
import java.util.*;

public class ConnectionManager {

    static Connection con;
    static String url;

    public static Connection getConnection() {

        try {
            String url = "jdbc:mysql://localhost:3306/hostnchill?useUnicode=true&characterEncoding=UTF-8&verifyServerCertificate=false&useSSL=true";
            Class.forName("com.mysql.jdbc.Driver");

            try {
                con = DriverManager.getConnection(url, "root", "toor");
            } catch (SQLException exception) {
                exception.printStackTrace();
            }
        } catch (ClassNotFoundException e) {
            System.out.println(e);
        }
        return  con;
    }
}
