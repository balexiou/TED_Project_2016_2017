/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Beans;

import java.util.ArrayList;
import java.util.Date;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(name= "booking")
public class BookBean {
    private int listingId;          // id of listing
    private int userId;		    // id of user that wants to book	
    private String listingThumbNail;//listing Thumbnail
    private String startDate;       //date booking starts
    private String endDate;	    //checkout date
    private int verify;			
    private ArrayList search_rs;

	//Getters and Setters

    public ArrayList getSearch_rs() {
        return search_rs;
    }
    @XmlElement
    public void setSearch_rs(ArrayList search_rs) {
        this.search_rs = search_rs;
    }

    public int getUserId() {
        return userId;
    }
    @XmlAttribute
    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getListingThumbNail() {
        return listingThumbNail;
    }
    @XmlElement
    public void setListingThumbNail(String listingThumbNail) {
        this.listingThumbNail = listingThumbNail;
    }
    
    public String getStartDate() {
        return startDate;
    }
    @XmlElement
    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }
    @XmlElement
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public int getVerify() {
        return verify;
    }
    @XmlElement
    public void setVerify(int verify) {
        this.verify = verify;
    }

    public int getListingId() {
        return listingId;
    }
    @XmlElement
    public void setListingId(int listingId) {
        this.listingId = listingId;
    }
    
}
