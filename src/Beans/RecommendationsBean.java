package Beans;

import java.util.ArrayList;

public class RecommendationsBean {
    private ArrayList<String> names;  
    private ArrayList<Integer> IDs;
    private ArrayList<Float> prices;
    private ArrayList<String> urls;

    public ArrayList<String> getNames() {
        return names;
    }

    public void setNames(ArrayList<String> names) {
        this.names = names;
    }

    public ArrayList<Integer> getIDs() {
        return IDs;
    }

    public void setIDs(ArrayList<Integer> IDs) {
        this.IDs = IDs;
    }

    public ArrayList<Float> getPrices() {
        return prices;
    }

    public void setPrices(ArrayList<Float> prices) {
        this.prices = prices;
    }

    public ArrayList<String> getUrls() {
        return urls;
    }

    public void setUrls(ArrayList<String> urls) {
        this.urls = urls;
    }

    
}
