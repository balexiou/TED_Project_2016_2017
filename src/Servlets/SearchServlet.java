package Servlets;

import Beans.SearchBean;
import Beans.UserBean;
import DAOs.SearchDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.ResultSet;

/**
 * Created by charis on 9/17/2017.
 */
@WebServlet(name = "SearchServlet")
public class SearchServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String start_date;
        String end_date;
        String date_range = request.getParameter("daterange");
        UserBean user;
        int user_id;
        String[] tokens = date_range.split(" ");
        start_date = tokens[0];
        end_date = tokens[2];

        try {
            HttpSession session = request.getSession(true);
            user = (UserBean) session.getAttribute("Info");
            System.out.println(user);

            if (session.getAttribute("Info") == null)
                user_id = 0;
            else
                user_id = user.getUserID();
            ResultSet search_rs = null;
            SearchBean search_bean = new SearchBean();

            search_bean.setUserId(user_id);
            search_bean.setLocation(request.getParameter("location"));
            System.out.println(request.getParameter("location"));
            search_bean.setStreet(request.getParameter("location"));
            search_bean.setNeighbourhood(request.getParameter("location"));
            search_bean.setAccomodates(request.getParameter("accomodates"));
            search_bean.setStartDate(start_date);
            search_bean.setEndDate(end_date);

            search_bean = SearchDAO.search(search_bean);
            session.setAttribute("current_search", search_bean);
            response.sendRedirect("searchResults.jsp");
        } catch (Throwable thException) {
            System.out.println(thException);
        }
    }
}
