package Servlets;

import Beans.ListingBean;
import DAOs.ListingDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by charis on 9/28/2017.
 */
@WebServlet(name = "EditListingServlet")
public class EditListingServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            ListingBean listingBean = new ListingBean();

            listingBean.setId(Integer.parseInt(request.getParameter("listingId")));
            System.out.println("listing_id = " + listingBean.getId());
            listingBean = ListingDAO.returnListingObject(listingBean);

            HttpSession session = request.getSession(true);
            session.setAttribute("EditListing", listingBean);
            response.sendRedirect("editListing.jsp");

        } catch (Throwable thException) {
            System.out.println("EditListingServlet Failed: " + thException);
        }
    }
}
