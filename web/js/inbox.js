/**
 * Created by charis on 9/28/2017.
 */

$('#inmessages').dataTable({
    "searching": false,
    columnDefs: [
        {type: 'datetime', targets: 2},
        {"orderable": false, "targets": [0, 1]}
    ],
    "order": [[2, 'desc']],
    stateSave: true
});
$('#outmessages').dataTable({
    "searching": false,
    columnDefs: [
        {type: 'datetime', targets: 2},
        {"orderable": false, "targets": [0, 1]}
    ],
    "order": [[2, 'desc']],
    "stateSave": true,
});
