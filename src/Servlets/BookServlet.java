package Servlets;

import Beans.BookBean;
import Beans.ListingBean;
import Beans.SearchBean;
import Beans.UserBean;
import DAOs.BookDAO;
import DAOs.UserDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Created by charis on 9/28/2017.
 */
@WebServlet(name = "BookServlet")
public class BookServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {
            DateFormat dateFormat = new SimpleDateFormat("YYYY-MM-DD");
            BookBean bookBean = new BookBean();
            SearchBean searchBean = null;
            ListingBean listingBean = null;
            UserBean userBean = null;
            HttpSession session = request.getSession(true);

            searchBean = (SearchBean) session.getAttribute("current_search");
            listingBean = (ListingBean) session.getAttribute("Listing");
            userBean = (UserBean) session.getAttribute("Info");
            session.setAttribute("Info", userBean);

            bookBean.setUserId(userBean.getUserID());
            bookBean.setListingId(listingBean.getId());
            bookBean.setListingThumbNail(listingBean.getThumbnail_url());
            bookBean.setStartDate(searchBean.getStartDate());
            bookBean.setEndDate(searchBean.getEndDate());

            bookBean = BookDAO.verifyBooking(bookBean);

            if (bookBean.getVerify() == 1) {
                System.out.println("Booking was successfully made!");
                bookBean = UserDAO.userBookings(bookBean, userBean);

                session.setAttribute("Bookings", bookBean);
                response.sendRedirect("user.jsp");
            }
        } catch (Throwable thException) {
            System.out.println("BookServlet Failed!: " + thException);
        }


    }

}
