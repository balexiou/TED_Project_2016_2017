<%--
  Created by IntelliJ IDEA.
  User: charis
  Date: 9/21/2017
  Time: 5:51 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="Beans.UserBean" %>
<%@ page import="Beans.UnverifiedUsersBean" %>
<%@ page import="java.util.ArrayList" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width-device-width, initial-scale1=1">
    <meta name="description" content="Vacation Rentals, Homes, Experiences & Places">
    <meta name="keywords" content="hospitality service, rent, lease, vacation rentals, apartment rentals, homestays, hostel, hostel beds, hotel rooms, experiences, places">
    <meta name="author" content="Babis Alexiou">

    <%
        UnverifiedUsersBean unverifiedUsersBean = (UnverifiedUsersBean) session.getAttribute("UnverifiedUsers");
        ArrayList<String> unverifiedUsers = null;
        if (unverifiedUsersBean != null) {
            unverifiedUsers = unverifiedUsersBean.getUnverifiedUsers_rs();
        }
        UserBean info = (UserBean) session.getAttribute("Info");
    %>

    <title>Host & Chill | Verify Users</title>
    <link rel="shortcut icon"  href="/favicon.ico?" />


    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="css/admin.css">
    <link rel="stylesheet" href="css/register.css">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css"/>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Oxygen" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">


    <!-- Latest compiled and minified JavaScript -->
    <script src="js/jquery.js"></script>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootsrap.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/user.js"></script>
    <script src="js/checkall.js"></script>
    <script src="js/validatePassword.js"></script>
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">
<%
    if (info.getIsAdmin()){
%>

<style>
    #brand_image {
        height: 30px;
    }

</style>
<!-- Navbat Starts Here -->
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="admin.jsp">
                <img id="brand_image" src="images/LOGO.jpg" alt="">
            </a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Welcome, <%= info.getFirst_name() %> <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><img src="<%=info.getPicture()%>" id="profile_image" class=" img-circle" alt=""> <%= info.getFirst_name() %></li>
                        <li class="divider"></li>
                        <li><a href="admin.jsp"><i class="fa fa-id-card-o" aria-hidden="true"></i> Admin Panel</a></li>
                        <li><a href="adminuserverify.jsp"><i class="fa fa-users" aria-hidden="true"></i> Verify Users</a></li>
                        <li><a href="adminExportXML.jsp"><i class="fa fa-download" aria-hidden="true"></i> Export XML</a></li>
                        <li class="divider"></li>
                        <li><a href="LogoutServlet/"><i class="fa fa-sign-out " aria-hidden="true"></i> Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
<!-- Navbar ends Here! -->

<div class="container text-center" style="margin-top: 40px">
    <%
        if (unverifiedUsers != null) {
            if (!unverifiedUsers.isEmpty()) {
    %>
    <form action="VerifyUsersServlet" method="post">
        <h3 style="font-family: 'Raleway', sans-serif; color: #fff">Unverified Users</h3>
        <hr/>
        <div class=" table-responsive">
            <table class="table">
                <thead>
                <tr>
                    <th style="font-family: 'Raleway', sans-serif; color: #fff">Username</th>
                </tr>
                </thead>
                <tbody>
                <%
                    for (int i = 0; i < unverifiedUsers.size(); i++) {

                %>
                <tr>
                    <td><a href="DisplayUserServlet?username=<%=unverifiedUsers.get(i)%>"><%=unverifiedUsers.get(i)%></a></td>
                    <td><input type="checkbox" name="UnverifiedUsers" value="<%=unverifiedUsers.get(i)%>"></td>
                </tr>
                <%
                    }
                %>
                </tbody>
            </table>
        </div>
        <hr/>
        <p align="center" style="font-family: 'Raleway', sans-serif; color: #fff">Select All <input type="checkbox" onclick="checkAll(this)"></p>
        <p align="center"><button type="submit" class="btn btn-primary btn-md login-button">Verify</button></p>
    </form>
    <%
        } else if (unverifiedUsers.isEmpty()){
    %>
    <h3 align="center" style="font-family: 'Raleway', sans-serif; color: #fff">All users are verified! <i class="fa fa-check-circle" aria-hidden="true" style="color: #3CB371;"></i></h3>
    <%
        } else {
    %>
    <h3 align="center" style="font-family: 'Raleway', sans-serif; color: #fff">Oops, something went wrong! <i class="fa fa-times-circle-o" aria-hidden="true" style="color: #fa4646;"></i></h3>
    <%
        }
    %>
</div>

<%
    }
    }
%>
<%
    if (!info.getIsAdmin()){
%>
<h1 align="center" style="font-family: 'Raleway', sans-serif; color: #fff ">Unauthorized Access</h1>
<%
    }
%>
</body>
</html>
