package Beans;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.Date;

/**
 * Created by Babis on 9/14/2017.
 */
@XmlRootElement(name = "listing")
public class ListingBean {

    private int id;
    private String listing_url;
    private String scrape_id;
    private String name;
    private String summary;
    private String space;
    private String description;
    private String experiences_offered;
    private String neighborhood_overview;
    private String notes;
    private String transit;
    private String thumbnail_url;
    private String medium_url;
    private String picture_url;
    private String xl_picture_url;
    private int host_id;
    private String host_url;
    private String host_name;
    private Date host_since;
    private String host_location;
    private String host_about;
    private String host_response_time;
    private String host_response_rate;
    private String host_acceptance_rate;
    private char host_is_superhost;
    private String host_thumbnail_url;
    private String host_picture_url;
    private String host_neighbourhood;
    private int host_listings_count;
    private int host_total_listings_count;
    private String host_verifications;
    private char host_has_profile_pic;
    private char host_identity_verified;
    private String street;
    private String neighbourhood;
    private String neighbourhood_cleansed;
    private String neighbourhood_group_cleansed;
    private String city;
    private String state;
    private String zipcode;
    private String market;
    private String country_code;
    private String country;
    private float latitude;
    private float longitude;
    private char is_location_exact;
    private String property_type;
    private String room_type;
    private int accommodates;
    private int bathrooms;
    private int bedrooms;
    private int beds;
    private String bed_type;
    private String amenities;
    private int square_feet;
    private float price;
    private float weekly_price;
    private float monthly_price;
    private float security_deposit;
    private float cleaning_fee;
    private int guests_included;
    private int extra_people;
    private int minimum_nights;
    private int maximum_nights;
    private String calendar_updated;
    private char has_availability;
    private int availability_30;
    private int availability_60;
    private int availability_90;
    private int availability_365;
    private Date calendar_last_scraped;
    private int number_of_reviews;
    private Date first_review;
    private Date last_review;
    private int review_scores_rating;
    private int review_scores_accuracy;
    private int review_scores_cleanliness;
    private int review_scores_checkin;
    private int review_scores_communication;
    private int review_scores_location;
    private int review_scores_value;
    private char requires_license;
    private String license;
    private String jurisdiction_names;
    private char instant_bookable;
    private String cancellation_policy;
    private char require_guest_profile_picture;
    private char require_guest_phone_verification;
    private int calculated_host_listings_count;
    private float reviews_per_month;

    public int getId() {
        return id;
    }

    public String getListing_url(){
        return listing_url;
    }

    public String getScrape_id() {
        return scrape_id;
    }

    public Date getCalendar_last_scraped(){
        return calendar_last_scraped;
    }

    public String getName(){
        return name;
    }

    public String getSummary(){
        return summary;
    }

    public String getSpace() {
        return space;
    }

    public String getDescription(){
        return description;
    }

    public String getExperiences_offered() {
        return experiences_offered;
    }

    public String getNeighborhood_overview() {
        return neighborhood_overview;
    }

    public char getHost_has_profile_pic() {
        return host_has_profile_pic;
    }

    public char getHost_identity_verified() {
        return host_identity_verified;
    }

    public char getHost_is_superhost() {
        return host_is_superhost;
    }

    public Date getHost_since() {
        return host_since;
    }

    public float getLatitude() {
        return latitude;
    }

    public char getIs_location_exact() {
        return is_location_exact;
    }

    public float getLongitude() {
        return longitude;
    }

    public float getMonthly_price() {
        return monthly_price;
    }

    public float getCleaning_fee() {
        return cleaning_fee;
    }

    public float getPrice() {
        return price;
    }

    public int getHost_id() {
        return host_id;
    }

    public float getSecurity_deposit() {
        return security_deposit;
    }

    public float getWeekly_price() {
        return weekly_price;
    }

    public int getAccommodates() {
        return accommodates;
    }

    public int getBathrooms() {
        return bathrooms;
    }

    public int getBedrooms() {
        return bedrooms;
    }

    public int getBeds() {
        return beds;
    }

    public int getExtra_people() {
        return extra_people;
    }

    public int getGuests_included() {
        return guests_included;
    }

    public int getHost_listings_count() {
        return host_listings_count;
    }

    public char getHas_availability() {
        return has_availability;
    }

    public int getHost_total_listings_count() {
        return host_total_listings_count;
    }

    public int getAvailability_30() {
        return availability_30;
    }

    public int getMaximum_nights() {
        return maximum_nights;
    }

    public int getAvailability_60() {
        return availability_60;
    }

    public int getMinimum_nights() {
        return minimum_nights;
    }

    public int getAvailability_90() {
        return availability_90;
    }

    public int getSquare_feet() {
        return square_feet;
    }

    public int getAvailability_365() {
        return availability_365;
    }

    public String getAmenities() {
        return amenities;
    }

    public String getBed_type() {
        return bed_type;
    }

    public int getNumber_of_reviews() {
        return number_of_reviews;
    }

    public String getCalendar_updated() {
        return calendar_updated;
    }

    public Date getFirst_review() {
        return first_review;
    }

    public String getCity() {
        return city;
    }

    public Date getLast_review() {
        return last_review;
    }

    public String getHost_name() {
        return host_name;
    }

    public int getReview_scores_rating() {
        return review_scores_rating;
    }

    public String getCountry() {
        return country;
    }

    public int getReview_scores_accuracy() {
        return review_scores_accuracy;
    }

    public String getCountry_code() {
        return country_code;
    }

    public int getReview_scores_cleanliness() {
        return review_scores_cleanliness;
    }

    public String getHost_about() {
        return host_about;
    }

    public int getReview_scores_checkin() {
        return review_scores_checkin;
    }

    public String getHost_acceptance_rate() {
        return host_acceptance_rate;
    }

    public String getHost_location() {
        return host_location;
    }

    public int getReview_scores_communication() {
        return review_scores_communication;
    }

    public int getReview_scores_location() {
        return review_scores_location;
    }

    public String getHost_neighbourhood() {
        return host_neighbourhood;
    }

    public int getReview_scores_value() {
        return review_scores_value;
    }

    public String getHost_picture_url() {
        return host_picture_url;
    }

    public char getRequires_license() {
        return requires_license;
    }

    public String getHost_response_rate() {
        return host_response_rate;
    }

    public String getHost_response_time() {
        return host_response_time;
    }

    public String getHost_thumbnail_url() {
        return host_thumbnail_url;
    }

    public String getHost_url() {
        return host_url;
    }

    public String getHost_verifications() {
        return host_verifications;
    }

    public char getInstant_bookable() {
        return instant_bookable;
    }

    public String getJurisdiction_names() {
        return jurisdiction_names;
    }

    public String getCancellation_policy() {
        return cancellation_policy;
    }

    public String getLicense() {
        return license;
    }

    public char getRequire_guest_profile_picture() {
        return require_guest_profile_picture;
    }

    public String getMarket() {
        return market;
    }

    public char getRequire_guest_phone_verification() {
        return require_guest_phone_verification;
    }

    public String getMedium_url() {
        return medium_url;
    }

    public int getCalculated_host_listings_count() {
        return calculated_host_listings_count;
    }

    public String getNeighbourhood() {
        return neighbourhood;
    }

    public String getNotes() {
        return notes;
    }

    public float getReviews_per_month() {
        return reviews_per_month;
    }

    public String getNeighbourhood_cleansed() {
        return neighbourhood_cleansed;
    }

    public String getNeighbourhood_group_cleansed() {
        return neighbourhood_group_cleansed;
    }

    public String getPicture_url() {
        return picture_url;
    }

    public String getProperty_type() {
        return property_type;
    }

    public String getRoom_type() {
        return room_type;
    }

    public String getState() {
        return state;
    }

    public String getStreet() {
        return street;
    }

    public String getThumbnail_url() {
        return thumbnail_url;
    }

    public String getTransit() {
        return transit;
    }

    public String getXl_picture_url() {
        return xl_picture_url;
    }

    public String getZipcode() {
        return zipcode;
    }

    @XmlElement
    public void setDescription(String description) {
        this.description = description;
    }

    @XmlAttribute
    public void setId(int id) {
        this.id = id;
    }

    public void setListing_url(String listing_url) {
        this.listing_url = listing_url;
    }

    @XmlElement
    public void setExperiences_offered(String experiences_offered) {
        this.experiences_offered = experiences_offered;
    }

    @XmlElement
    public void setName(String name) {
        this.name = name;
    }

    @XmlElement
    public void setNeighborhood_overview(String neighborhood_overview) {
        this.neighborhood_overview = neighborhood_overview;
    }

    public void setScrape_id(String scrape_id) {
        this.scrape_id = scrape_id;
    }

    public void setSpace(String space) {
        this.space = space;
    }

    @XmlElement
    public void setNotes(String notes) {
        this.notes = notes;
    }

    @XmlElement
    public void setSummary(String summary) {
        this.summary = summary;
    }

    @XmlElement
    public void setCity(String city) {
        this.city = city;
    }

    @XmlElement
    public void setCountry(String country) {
        this.country = country;
    }

    public void setHost_about(String host_about) {
        this.host_about = host_about;
    }

    @XmlElement
    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }

    public void setHost_acceptance_rate(String host_acceptance_rate) {
        this.host_acceptance_rate = host_acceptance_rate;
    }

    public void setHost_has_profile_pic(char host_has_profile_pic) {
        this.host_has_profile_pic = host_has_profile_pic;
    }

    @XmlElement
    public void setHost_id(int host_id) {
        this.host_id = host_id;
    }

    public void setHost_identity_verified(char host_identity_verified) {
        this.host_identity_verified = host_identity_verified;
    }

    public void setHost_is_superhost(char host_is_superhost) {
        this.host_is_superhost = host_is_superhost;
    }

    public void setHost_listings_count(int host_listings_count) {
        this.host_listings_count = host_listings_count;
    }

    public void setHost_location(String host_location) {
        this.host_location = host_location;
    }

    @XmlElement
    public void setHost_name(String host_name) {
        this.host_name = host_name;
    }

    public void setHost_neighbourhood(String host_neighbourhood) {
        this.host_neighbourhood = host_neighbourhood;
    }

    public void setHost_picture_url(String host_picture_url) {
        this.host_picture_url = host_picture_url;
    }

    public void setHost_response_rate(String host_response_rate) {
        this.host_response_rate = host_response_rate;
    }

    public void setHost_response_time(String host_response_time) {
        this.host_response_time = host_response_time;
    }

    public void setHost_since(Date host_since) {
        this.host_since = host_since;
    }

    public void setHost_thumbnail_url(String host_thumbnail_url) {
        this.host_thumbnail_url = host_thumbnail_url;
    }

    public void setHost_total_listings_count(int host_total_listings_count) {
        this.host_total_listings_count = host_total_listings_count;
    }

    public void setHost_url(String host_url) {
        this.host_url = host_url;
    }

    public void setHost_verifications(String host_verifications) {
        this.host_verifications = host_verifications;
    }

    public void setMedium_url(String medium_url) {
        this.medium_url = medium_url;
    }

    public void setPicture_url(String picture_url) {
        this.picture_url = picture_url;
    }

    public void setThumbnail_url(String thumbnail_url) {
        this.thumbnail_url = thumbnail_url;
    }

    @XmlElement
    public void setNeighbourhood(String neighbourhood) {
        this.neighbourhood = neighbourhood;
    }

    public void setNeighbourhood_cleansed(String neighbourhood_cleansed) {
        this.neighbourhood_cleansed = neighbourhood_cleansed;
    }

    @XmlElement
    public void setMarket(String market) {
        this.market = market;
    }

    public void setNeighbourhood_group_cleansed(String neighbourhood_group_cleansed) {
        this.neighbourhood_group_cleansed = neighbourhood_group_cleansed;
    }

    @XmlElement
    public void setState(String state) {
        this.state = state;
    }

    @XmlElement
    public void setStreet(String street) {
        this.street = street;
    }

    @XmlElement
    public void setTransit(String transit) {
        this.transit = transit;
    }

    public void setXl_picture_url(String xl_picture_url) {
        this.xl_picture_url = xl_picture_url;
    }

    @XmlElement
    public void setAccommodates(int accommodates) {
        this.accommodates = accommodates;
    }

    @XmlElement
    public void setAmenities(String amenities) {
        this.amenities = amenities;
    }

    public void setAvailability_30(int availability_30) {
        this.availability_30 = availability_30;
    }

    public void setAvailability_60(int availability_60) {
        this.availability_60 = availability_60;
    }

    public void setAvailability_90(int availability_90) {
        this.availability_90 = availability_90;
    }

    public void setAvailability_365(int availability_365) {
        this.availability_365 = availability_365;
    }

    @XmlElement
    public void setBathrooms(int bathrooms) {
        this.bathrooms = bathrooms;
    }

    @XmlElement
    public void setBed_type(String bed_type) {
        this.bed_type = bed_type;
    }

    @XmlElement
    public void setBedrooms(int bedrooms) {
        this.bedrooms = bedrooms;
    }

    @XmlElement
    public void setBeds(int beds) {
        this.beds = beds;
    }

    public void setCalendar_updated(String calendar_updated) {
        this.calendar_updated = calendar_updated;
    }

    public void setCalendar_last_scraped(Date calendar_last_scraped) {
        this.calendar_last_scraped = calendar_last_scraped;
    }

    @XmlElement
    public void setCleaning_fee(float cleaning_fee) {
        this.cleaning_fee = cleaning_fee;
    }

    public void setExtra_people(int extra_people) {
        this.extra_people = extra_people;
    }

    @XmlElement
    public void setGuests_included(int guests_included) {
        this.guests_included = guests_included;
    }

    public void setHas_availability(char has_availability) {
        this.has_availability = has_availability;
    }

    public void setIs_location_exact(char is_location_exact) {
        this.is_location_exact = is_location_exact;
    }

    @XmlElement
    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    @XmlElement
    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    @XmlElement
    public void setMaximum_nights(int maximum_nights) {
        this.maximum_nights = maximum_nights;
    }

    @XmlElement
    public void setMinimum_nights(int minimum_nights) {
        this.minimum_nights = minimum_nights;
    }

    public void setMonthly_price(float monthly_price) {
        this.monthly_price = monthly_price;
    }

    @XmlElement
    public void setPrice(float price) {
        this.price = price;
    }

    @XmlElement
    public void setProperty_type(String property_type) {
        this.property_type = property_type;
    }

    public void setFirst_review(Date first_review) {
        this.first_review = first_review;
    }

    @XmlElement
    public void setRoom_type(String room_type) {
        this.room_type = room_type;
    }

    public void setSecurity_deposit(float security_deposit) {
        this.security_deposit = security_deposit;
    }

    public void setLast_review(Date last_review) {
        this.last_review = last_review;
    }

    @XmlElement
    public void setSquare_feet(int square_feet) {
        this.square_feet = square_feet;
    }

    public void setNumber_of_reviews(int number_of_reviews) {
        this.number_of_reviews = number_of_reviews;
    }

    public void setWeekly_price(float weekly_price) {
        this.weekly_price = weekly_price;
    }

    @XmlElement
    public void setZipcode(String zipcode) {
        this.zipcode = zipcode;
    }

    public void setCalculated_host_listings_count(int calculated_host_listings_count) {
        this.calculated_host_listings_count = calculated_host_listings_count;
    }

    public void setCancellation_policy(String cancellation_policy) {
        this.cancellation_policy = cancellation_policy;
    }

    public void setInstant_bookable(char instant_bookable) {
        this.instant_bookable = instant_bookable;
    }

    public void setJurisdiction_names(String jurisdiction_names) {
        this.jurisdiction_names = jurisdiction_names;
    }

    public void setLicense(String license) {
        this.license = license;
    }

    public void setRequire_guest_phone_verification(char require_guest_phone_verification) {
        this.require_guest_phone_verification = require_guest_phone_verification;
    }

    public void setRequire_guest_profile_picture(char require_guest_profile_picture) {
        this.require_guest_profile_picture = require_guest_profile_picture;
    }

    public void setRequires_license(char requires_license) {
        this.requires_license = requires_license;
    }

    public void setReview_scores_accuracy(int review_scores_accuracy) {
        this.review_scores_accuracy = review_scores_accuracy;
    }

    public void setReview_scores_checkin(int review_scores_checkin) {
        this.review_scores_checkin = review_scores_checkin;
    }

    public void setReview_scores_cleanliness(int review_scores_cleanliness) {
        this.review_scores_cleanliness = review_scores_cleanliness;
    }

    public void setReview_scores_communication(int review_scores_communication) {
        this.review_scores_communication = review_scores_communication;
    }

    public void setReview_scores_location(int review_scores_location) {
        this.review_scores_location = review_scores_location;
    }

    public void setReview_scores_rating(int review_scores_rating) {
        this.review_scores_rating = review_scores_rating;
    }

    public void setReview_scores_value(int review_scores_value) {
        this.review_scores_value = review_scores_value;
    }

    public void setReviews_per_month(float reviews_per_month) {
        this.reviews_per_month = reviews_per_month;
    }
}
