package Servlets;

import Beans.UserBean;
import DAOs.UserDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by Babis on 9/15/2017.
 */
@WebServlet(name = "DisplayUserServlet")
public class DisplayUserServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            UserBean user = new UserBean();
            //BookBean bookings = new BookBean();

            if (request.getParameter("username") == null) {
                user.setUserID(Integer.parseInt(request.getParameter("id")));
            }
            else {
                user.setUsername(request.getParameter("username"));
            }
            System.out.println("The user being displayed is " + user.getUsername());

            if (request.getParameter("username") == null) {
                user = UserDAO.displayUserID(user);
            }
            else {
                user = UserDAO.displayUser(user);
            }

            HttpSession session = request.getSession(true);
            session.setAttribute("Info", user);

            response.sendRedirect("user.jsp");

        } catch (Throwable thException) {
            System.out.println(thException);
        }
    }
}
