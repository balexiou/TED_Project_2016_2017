/**
 * Created by Babis on 9/18/2017.
 */

function validatePassword () {
    var password = document.forms["register"]["password"].value;
    var repeat_password = document.forms["register"]["rpassword"].value;

    if (password !== repeat_password ) {
        alert("Passwords do not match! Try again!");
        return false;
    }

    return true;
}

function validateUPassword() {
    var password = document.forms["update"]["password"].value;
    var repeat_password = document.forms["update"]["rpassword"].value;

    if (password !== repeat_password ) {
        alert("Passwords do not match! Try again!");
        return false;
    }

    return true;
}
