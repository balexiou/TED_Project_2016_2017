<%--
  Created by IntelliJ IDEA.
  User: charis
  Date: 9/28/2017
  Time: 11:36 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="Beans.UserBean" %>
<%@ page import="Beans.AllMessagesBean" %>
<%@ page import="java.util.ArrayList" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width-device-width, initial-scale1=1">
    <meta name="description" content="Vacation Rentals, Homes, Experiences & Places">
    <meta name="keywords" content="hospitality service, rent, lease, vacation rentals, apartment rentals, homestays, hostel, hostel beds, hotel rooms, experiences, places">
    <meta name="author" content="Babis Alexiou">

    <title>Inbox | Host & Chill</title>
    <link rel="shortcut icon"  href="/favicon.ico?" />

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="css/admin.css">
    <link rel="stylesheet" href="css/register.css">
    <link rel="stylesheet" href="css/addListing.css">
    <link rel="stylesheet" href="css/inbox.css">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css"/>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Oxygen" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">

    <!-- Latest compiled and minified JavaScript -->
    <script src="js/jquery.js"></script>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootsrap.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/user.js"></script>
    <script src="js/validatePassword.js"></script>
    <script src="js/datepicker.js"></script>
    <script src="js/navigation.js"></script>
    <script src="js/back_to_top.js"></script>
    <script src="js/datasort.js"></script>
    <script src="js/inbox.js"></script>

    <%
        UserBean info = (UserBean) session.getAttribute("Info");
    %>
</head>
<body>
<%
    if (info != null) {
%>
<div class="container">
    <div class="row main">
        <div class="panel-heading">
            <div class="panel-title text-center">
                <h1 class="title"> Host & Chill </h1>
                <hr/>
                <h2 class="title">Inbox</h2>
            </div>
        </div>
        <%
            AllMessagesBean allMessagesBean = (AllMessagesBean) session.getAttribute("messages");
            ArrayList incoming = allMessagesBean.getIn_messages();
        %>
        <h2 class="title">Incoming</h2>
        <hr/>
        <%
            if (!incoming.isEmpty()) {

        %>
        <table id="inmessages" class="table table-responsive" style="table-layout: auto">
            <thead>
            <tr style="font-family: Raleway, sans-serif; color: #fff">
                <th>Sender</th>
                <th>Message</th>
                <th>Received</th>
            </tr>
            </thead>
            <%
                for (int i = 0; i < incoming.size(); i++) {
                    ArrayList incomingInfo = (ArrayList) incoming.get(i);
            %>
            <tr style="font-family: Raleway, sans-serif; color: #fff">
                <th <%if ((Boolean) incomingInfo.get(0)) {%> style="font-weight:normal"<%}%>><%=incomingInfo.get(1)%></th>
                <th style="width:60%"><a href="MessageSeenServlet?messageID=<%=incomingInfo.get(5)%>"><preview <%if ((Boolean) incomingInfo.get(0)) {%> style="font-weight:normal; color: #fff"<%}%>><%=incomingInfo.get(2)%></preview></a></th>
                <th <%if ((Boolean) incomingInfo.get(0)) {%> style="font-weight:normal"<%}%>><%=incomingInfo.get(3)%> <%=incomingInfo.get(4)%></th>
            </tr>
            <%
                }
            %>
        </table>
        <%
            } else{
        %>
        <h5 align="center" style="color: #fff">You have no Incoming messages! :(</h5>
        <%
            }
        %>
        <h2 class="title">Outgoing</h2>
        <%
            ArrayList outgoing = allMessagesBean.getOut_messages();
            if (!outgoing.isEmpty()){
        %>
        <table id="outmessages" class="table table-responsive" style="table-layout: auto">
            <thead>
            <tr style="font-family: Raleway, sans-serif; color: #fff">
                <th >Receiver</th>
                <th>Message</th>
                <th>Sent</th>
            </tr>
            </thead>
            <%
                for (int i = 0; i < outgoing.size(); i++) {
                    ArrayList outgoingInfo = (ArrayList) outgoing.get(i);
            %>
            <tr style="font-family: Raleway, sans-serif; color: #fff">
                <th style="font-weight: normal"><%=outgoingInfo.get(0)%></th>
                <th style="width: 60%; font-weight: normal"><preview><%=outgoingInfo.get(1)%></preview></th>
                <th style="font-weight: normal"><%=outgoingInfo.get(2)%> <%=outgoingInfo.get(3)%></th>
            </tr>
            <%
                }
            %>
        </table>
        <%
        } else{
        %>
        <h5 align="center" style="color: #fff">You have no Outgoing messages! :(</h5>
        <%
            }
        %>
        <%
            if (info.getIsAdmin()) {
        %>
        <a href="admin.jsp">
            <h5 style="font-family: Raleway, sans-serif; color: #fff" align="center">Back</h5>
        </a>
        <%
            } else {
        %>
        <a href="user.jsp">
            <h5 style="font-family: Raleway, sans-serif; color: #fff" align="center">Back</h5>
        </a>
        <%
            }
        %>
    </div>
</div>

<%
    } else{
%>
<!-- Login or Register Message -->
<div class="container">
    <div class="row main">
        <div class="panel-heading">
            <div class="panel-title text-center">
                <h1 class="title"> Host & Chill </h1>
                <hr/>
                <h2 class="title">Register or Login!</h2>
            </div>
        </div>
    </div>
</div>
<%
    }
%>
</body>
</html>
