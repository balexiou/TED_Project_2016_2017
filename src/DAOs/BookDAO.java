package DAOs;

import Beans.BookBean;
import DB.ConnectionManager;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

/**
 * Created by charis on 9/17/2017.
 */
public class BookDAO {

    static Connection current_connnection = null;
    static ResultSet rs =null;

    public static BookBean verifyBooking(BookBean booking) {
        PreparedStatement stmt = null;
        PreparedStatement update_stmt = null;

        int affected_rows;
        int insert_affected_rows;

        int listing_id = booking.getListingId();
        int user_id = booking.getUserId();

        String listing_thumbnail = booking.getListingThumbNail();
        String start_date = booking.getStartDate();
        String end_date = booking.getEndDate();

        String search_query = "select * from calendar where listing_id = ? AND date >=  ? AND date <= ? for update";
        String update_query = "update calendar set available = ? where listing_id = ? AND date >=  ? AND date <= ?";

        try {
            current_connnection = ConnectionManager.getConnection();
            stmt = current_connnection.prepareStatement(search_query);
            stmt.setInt(1, listing_id);
            stmt.setString(2, start_date);
            stmt.setString(3,end_date);
            System.out.println(stmt);

            rs = stmt.executeQuery();
            if (rs.next()) {
                update_stmt = current_connnection.prepareStatement(update_query);
                update_stmt.setString(1, "f");
                update_stmt.setInt(2, listing_id);
                update_stmt.setString(3, start_date);
                update_stmt.setString(4, end_date);

                affected_rows = update_stmt.executeUpdate();
                System.out.println(update_stmt);
                System.out.println(affected_rows);

                booking.setVerify(1);

                String insert_query = "INSERT INTO bookings (user_id, listing_id, listing_url, start_date, end_date) "
                        + "VALUES ("+user_id+","+listing_id+",'"+listing_thumbnail+"','"+start_date+"','"+end_date+"')";

                System.out.println(insert_query);
                stmt.executeUpdate(insert_query);
                System.out.println("Success: Insert into bookings!");
            }
            else {
                System.out.println("Error: Insert into bookings!");
            }
        } catch (Exception ex){
            System.out.println("verifyBooking failed: An Exception has occured! " + ex);
        }
        finally {
            if (rs != null){
               try {
                   rs.close();
               } catch (Exception e) {
               }
               rs = null;
            }

            if (stmt != null){
                try {
                    stmt.close();
                } catch (Exception e) {
                }
                stmt = null;
            }

            if (update_stmt != null){
                try {
                    update_stmt.close();
                } catch (Exception e) {
                }
                update_stmt = null;
            }

            if (current_connnection != null){
                try {
                    current_connnection.close();
                } catch (Exception e) {
                }
                current_connnection = null;
            }
        }
        return  booking;
    }

    public static Boolean userNullBookings(int userId) {
        PreparedStatement stmt = null;
        Boolean user_null_bookins = true;
        String search_query = "select * from bookings where user_id = ? ";

        try {
            current_connnection = ConnectionManager.getConnection();
            stmt = current_connnection.prepareStatement(search_query);
            stmt.setInt(1, userId);
            System.out.println(stmt);
            rs = stmt.executeQuery();

            if (rs.next()) {
                System.out.println("User has previous bookings");
                user_null_bookins = false;
            }
            else {
                System.out.println("User has no previous bookings");
                user_null_bookins = true;
            }
        } catch (Exception ex){
            System.out.println("userNullBookings Error: An Exception has occured! " + ex);
        }
        finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
                rs = null;
            }

            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e) {
                }
                stmt = null;
            }

            if (current_connnection != null) {
                try {
                    current_connnection.close();
                } catch (Exception e) {
                }
                current_connnection = null;
            }
        }
        return user_null_bookins;
    }
}
