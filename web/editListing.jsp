<%@ page import="Beans.UserBean" %>
<%@ page import="Beans.ListingBean" %><%--
  Created by IntelliJ IDEA.
  User: charis
  Date: 9/28/2017
  Time: 3:32 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width-device-width, initial-scale1=1">
    <meta name="description" content="Vacation Rentals, Homes, Experiences & Places">
    <meta name="keywords" content="hospitality service, rent, lease, vacation rentals, apartment rentals, homestays, hostel, hostel beds, hotel rooms, experiences, places">
    <meta name="author" content="Babis Alexiou">

    <title>Edit Listing | Host & Chill</title>
    <link rel="shortcut icon"  href="/favicon.ico?" />

    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="css/admin.css">
    <link rel="stylesheet" href="css/register.css">
    <link rel="stylesheet" href="css/addListing.css">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css"/>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Oxygen" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">

    <!-- Latest compiled and minified JavaScript -->
    <script src="js/jquery.js"></script>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootsrap.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/user.js"></script>
    <script src="js/validatePassword.js"></script>
    <script src="js/datepicker.js"></script>
    <script src="js/navigation.js"></script>
    <script src="js/back_to_top.js"></script>

    <%
        UserBean info = (UserBean) session.getAttribute("Info");
        ListingBean listingBean = (ListingBean) session.getAttribute("EditListing");
    %>
</head>
<body>
<%
    if (info.getIsHost() && info != null) {
%>
<!-- Add Listing Form Starts Here -->
<div class="container">
    <div class="row main">
        <div class="panel-heading">
            <div class="panel-title text-center">
                <h1 class="title"> Host & Chill </h1>
                <hr/>
                <h2 class="title">Edit Listing</h2>
            </div>
        </div>
        <div class="main-center main-login" style="font-family: 'Raleway', sans-serif;">
            <form class="form-horizontal" name="editListing" method="post" action="VerifyListingEditServlet" enctype="multipart/form-data">
                <!-- Street Address -->
                <div class="form-group">
                    <label class="cols-sm-2 control-label">Street Address</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-map-marker fa" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" name="street" id="address" placeholder="Street Address" value="<%=listingBean.getStreet()%>">
                            <span class="input-group-btn"><input style="text-transform: none" id="submit" class="btn btn-danger btn-md" type="button" value="Search"></span>
                        </div>
                    </div>
                </div>
                <!-- Map -->
                <div class="form-group">
                    <label class="cols-sm-2 control-label">Map</label>
                    <div class="cols-sm-10">
                        <div id="map" class="container">
                        </div>
                    </div>
                </div>
                <!-- Name -->
                <div class="form-group">
                    <label class="cols-sm-2 control-label">Listing Title</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-address-card" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" name="name" placeholder="Title" value="<%=listingBean.getName()%>">
                        </div>
                    </div>
                </div>
                <!-- Neighbourhood -->
                <div class="form-group">
                    <label class="cols-sm-2 control-label">Neighbourhood</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-address-card" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" name="neighbourhood" placeholder="Neighbourhood" value="<%=listingBean.getNeighbourhood()%>">
                        </div>
                    </div>
                </div>
                <!-- City -->
                <div class="form-group">
                    <label class="cols-sm-2 control-label">City</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-building" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" name="city" placeholder="City" value="<%=listingBean.getCity()%>">
                        </div>
                    </div>
                </div>
                <!-- State -->
                <div class="form-group">
                    <label class="cols-sm-2 control-label">State</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-compass" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" name="state" placeholder="State" value="<%=listingBean.getState()%>">
                        </div>
                    </div>
                </div>
                <!-- Country -->
                <div class="form-group">
                    <label class="cols-sm-2 control-label">Country</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-flag" aria-hidden="true"></i></span>
                            <input type="text" class="form-control" name="country" placeholder="Country" value="<%=listingBean.getCountry()%>">
                        </div>
                    </div>
                </div>
                <!-- Room Type -->
                <div class="form-group">
                    <label class="cols-sm-2 control-label">Room Type</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <select name="room_type">
                                <option value="<%=listingBean.getRoom_type()%>" selected="selected"><%=listingBean.getRoom_type()%></option>
                                <option value="Entire home/apt">Entire Home / Apartment</option>
                                <option value="Private room">Private Room</option>
                                <option value="Shared room">Shared Room</option>
                            </select>
                        </div>
                    </div>
                </div>
                <!-- Price -->
                <div class="form-group">
                    <label class="cols-sm-2 control-label">Daily Fee</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-dollar" aria-hidden="true"></i></span>
                            <input type="number" step="1" class="form-control" name="price" value="<%=listingBean.getPrice()%>">
                        </div>
                    </div>
                </div>
                <!-- Accommodates -->
                <div class="form-group">
                    <label class="cols-sm-2 control-label">Number of Guests</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-users" aria-hidden="true"></i></span>
                            <input type="number" step="1" class="form-control" name="accommodates" value="<%=listingBean.getAccommodates()%>">
                        </div>
                    </div>
                </div>
                <!-- Extra cost -->
                <div class="form-group">
                    <label class="cols-sm-2 control-label">Extra Cost (per Guest)</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-plus" aria-hidden="true"></i></span>
                            <input type="number" step="1" class="form-control" name="extra_cost" value="<%=listingBean.getExtra_people()%>">
                        </div>
                    </div>
                </div>
                <!-- Minimum Nights -->
                <div class="form-group">
                    <label class="cols-sm-2 control-label">Minimum Nights</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-moon-o" aria-hidden="true"></i></span>
                            <input type="number" step="1" class="form-control" name="minimum_nights" value="<%=listingBean.getMinimum_nights()%>">
                        </div>
                    </div>
                </div>
                <!-- Availability -->
                <div class="form-group">
                    <label class="cols-sm-2 control-label">Availability</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-calendar" aria-hidden="true"></i></span>
                            <input type="date" class="form-control" name="available" placeholder="YYYY-MM-DD" required>
                        </div>
                    </div>
                </div>
                <!-- Bedrooms -->
                <div class="form-group">
                    <label class="cols-sm-2 control-label">Number of Bedrooms</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-bed" aria-hidden="true"></i></span>
                            <input type="number" step="1" class="form-control" name="bedrooms" value="<%=listingBean.getBedrooms()%>">
                        </div>
                    </div>
                </div>
                <!-- Beds -->
                <div class="form-group">
                    <label class="cols-sm-2 control-label">Number of Beds</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-bed" aria-hidden="true"></i></span>
                            <input type="number" step="1" class="form-control" name="beds" value="<%=listingBean.getBeds()%>">
                        </div>
                    </div>
                </div>
                <!-- Bathrooms -->
                <div class="form-group">
                    <label class="cols-sm-2 control-label">Number of Bathrooms</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-bath" aria-hidden="true"></i></span>
                            <input type="number" step="1" class="form-control" name="bathrooms" value="<%=listingBean.getBathrooms()%>">
                        </div>
                    </div>
                </div>
                <!-- Area -->
                <div class="form-group">
                    <label class="cols-sm-2 control-label">Listing Area (square ft)</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-square-o" aria-hidden="true"></i></span>
                            <input type="number" step="0.10" class="form-control" name="square_feet" value="<%=listingBean.getSquare_feet()%>">
                        </div>
                    </div>
                </div>
                <!-- Transit -->
                <div class="form-group">
                    <label class="cols-sm-2 control-label">Transit</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-bus" aria-hidden="true"></i></span>
                            <textarea name="transit" cols="30" rows="10" value=""><%=listingBean.getTransit()%></textarea>
                        </div>
                    </div>
                </div>
                <!-- Description -->
                <div class="form-group">
                    <label class="cols-sm-2 control-label">Description</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <span class="input-group-addon"><i class="fa fa-info" aria-hidden="true"></i></span>
                            <textarea name="description"  minlength="1" maxlength="1000" required><%=listingBean.getDescription()%></textarea>
                        </div>
                    </div>
                </div>
                <!-- Amenities -->
                <div class="form-group">
                    <label class="cols-sm-2 control-label">Amenities</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <p>Smoking: Allowed <input type="checkbox" name="amenities" value="Smoking Allowed"></p>
                            <p>Pets: Allowed <input type="checkbox" name="amenities" value="Pets Allowed"></p>
                            <p>Events: Allowed <input type="checkbox" name="amenities" value="Suitable for Events"></p>
                        </div>
                    </div>
                </div>
                <!-- Picture -->
                <div class="form-group">
                    <label class="cols-sm-2 control-label">Picture</label>
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <img src="<%=listingBean.getMedium_url()%>" class="img-thumbnail" style="object-fit: contain; width: 185px; height: 120px;">
                            <input type="file" name="photo" value="<%=listingBean.getMedium_url()%>">
                        </div>
                    </div>
                </div>
                <!-- Form Fields End Here -->
                <!-- Submit Button -->
                <div class="form-group">
                    <button type="submit" class="btn btn-primary btn-lg btn-block login-button">Submit</button>
                </div>
                <!-- Login Link -->
                <div class="login-register">
                    <a href="user.jsp">Back</a>
                </div>
                <!-- Latitude -->
                <div class="form-group">
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <input type="hidden" name="lat" id="lat" value="<%=listingBean.getLatitude()%>">
                        </div>
                    </div>
                </div>
                <!-- Longitude -->
                <div class="form-group">
                    <div class="cols-sm-10">
                        <div class="input-group">
                            <input type="hidden" name="lng" id="lng" value="<%=listingBean.getLongitude()%>">
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<%
    } if (!info.getIsHost()) {
%>
<div class="container">
    <div class="row main">
        <div class="panel-heading">
            <div class="panel-title text-center">
                <h1 class="title"> Host & Chill </h1>
                <hr/>
                <h2 class="title">Unauthorized Access</h2>
            </div>
        </div>
    </div>
</div>
<%
    } if (info == null) {
%>
<div class="container">
    <div class="row main">
        <div class="panel-heading">
            <div class="panel-title text-center">
                <h1 class="title"> Host & Chill </h1>
                <hr/>
                <h2 class="title">Register or Login!</h2>
            </div>
        </div>
    </div>
</div>
<%
    }
%>

<script async defer type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDHOVBagaelUjIKq_4IvDT9dDkYnqQvXDY&callback=initMap"></script>
<script>
    var marker;
    var lat, lng;
    var myLatLng = {lat: <%=listingBean.getLatitude()%>, lng: <%=listingBean.getLongitude()%>};


    function initMap() {
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 15,
            center: myLatLng
        });
        marker = new google.maps.Marker({
            position: myLatLng,
            map: map,
            draggable: true
        });

        var geocoder = new google.maps.Geocoder();

        document.getElementById('submit').addEventListener('click', function () {
            geocodeAddress(geocoder, map);
        });

        map.addListener('click', function (e) {
            placeMarkerAndPanTo(e.latLng, map);
        });

    }

    function geocodeAddress(geocoder, resultsMap) {
        var address = document.getElementById('address').value;
        geocoder.geocode({'address': address}, function (results, status) {
            if (status === 'OK') {
                resultsMap.setCenter(results[0].geometry.location);
                if (marker) {
                    marker.setPosition(results[0].geometry.location);
                } else {
                    marker = new google.maps.Marker({
                        map: resultsMap,
                        position: results[0].geometry.location,
                        draggable: true
                    });
                }
                lat = results[0].geometry.location.lat();//lat
                lng = results[0].geometry.location.lng();//lng
                document.getElementById('lat').value = lat;
                document.getElementById('lng').value = lng;
                console.log(lat);
                console.log(lng);
            } else {
                alert('Geocode was not successful for the following reason: ' + status);
            }
        });
    }

    function placeMarkerAndPanTo(latLng, map) {
        if (marker) {
            marker.setPosition(latLng);
        } else {
            marker = new google.maps.Marker({
                position: latLng,
                map: map,
                draggable: true
            });
            map.panTo(latLng);
        }
        lat = latLng.lat();
        lng = latLng.lng();
        document.getElementById('lat').value = lat;
        document.getElementById('lng').value = lng;
        //console.log(lat);
        //console.log(lng);
        console.log(document.getElementById('lat').value);
        console.log(document.getElementById('lng').value);
    }
</script>
<!-- Plugin JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"></script>

<!-- Include Date Range Picker -->
<script type="text/javascript" src="//cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.js"></script>
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/bootstrap.daterangepicker/2/daterangepicker.css" />
<script>
    $(function () {
        $('input[name="available"]').daterangepicker({
            locale: {
                separator: " / ",
                format: 'YYYY-MM-DD'
            },
        });
    });
</script>
</body>
</html>
