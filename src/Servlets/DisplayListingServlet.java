package Servlets;

import Beans.ListingBean;
import DAOs.ListingDAO;

import static java.lang.Integer.parseInt;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Created by Babis on 9/19/2017.
 */
@WebServlet(name = "DisplayListingServlet")
public class DisplayListingServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            ListingBean listingBean = new ListingBean();
            listingBean.setId(parseInt(request.getParameter("id")));
            System.out.println("listing_id: " + listingBean.getId());
            listingBean = ListingDAO.returnListingObject(listingBean);

            HttpSession session = request.getSession(true);
            session.setAttribute("Listing", listingBean);
            response.sendRedirect("listing.jsp");
        }
        catch (Throwable theException) {
            System.out.println(theException);
        }
    }
}
