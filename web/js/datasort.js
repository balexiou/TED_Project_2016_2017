/**
 * Created by charis on 9/28/2017.
 */
jQuery.extend(jQuery.fn.dataTableExt.oSort, {
    "datetime-asc": function (a, b) {
        var x, y;
        if (jQuery.trim(a) !== '') {
            var Datea = jQuery.trim(a).split(' ');
            var Timea = Datea[1].split(':');
            var Datea2 = Datea[0].split('-');
            x = (Datea2[0] + Datea2[1] + Datea2[1] + Timea[0] + Timea[1] + Timea[2]) * 1;
        } else {
            x = Infinity; // = l'an 1000 ...
        }

        if (jQuery.trim(b) !== '') {
            var Dateb = jQuery.trim(b).split(' ');
            var Timeb = Dateb[1].split(':');
            Dateb = Dateb[0].split('-');
            y = (Dateb[0] + Dateb[1] + Dateb[2] + Timeb[0] + Timeb[1] + Timeb[2]) * 1;
        } else {
            y = Infinity;
        }
        var z = ((x < y) ? -1 : ((x > y) ? 1 : 0));
        return z;
    },

    "datetime-desc": function (a, b) {
        var x, y;
        if (jQuery.trim(a) !== '') {
            var Datea = jQuery.trim(a).split(' ');
            var Timea = Datea[1].split(':');
            var Datea2 = Datea[0].split('-');
            x = (Datea2[0] + Datea2[1] + Datea2[2] + Timea[0] + Timea[1] + Timea[2]) * 1;
        } else {
            x = Infinity;
        }

        if (jQuery.trim(b) !== '') {
            var Dateb = jQuery.trim(b).split(' ');
            var Timeb = Dateb[1].split(':');
            Dateb = Dateb[0].split('-');
            y = (Dateb[0] + Dateb[1] + Dateb[2] + Timeb[0] + Timeb[1] + Timeb[2]) * 1;
        } else {
            y = Infinity;
        }
        var z = ((x < y) ? 1 : ((x > y) ? -1 : 0));
        return z;
    }
});
