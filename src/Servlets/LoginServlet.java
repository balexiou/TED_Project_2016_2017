package Servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Objects;

import Beans.BookBean;
import DAOs.UserDAO;
import Beans.UserBean;

/**
 * Created by Babis on 5/25/2017.
 */

@WebServlet(name = "LoginServlet")
public class LoginServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        try {

            UserBean user = new UserBean();
            UserBean info = new UserBean();
            BookBean bookings = new BookBean();
            user.setUsername(request.getParameter("username"));
            user.setPassword(request.getParameter("password"));

            user = UserDAO.login(user);
            info = UserDAO.displayUser(user);
            bookings = UserDAO.userBookings(bookings, user);

            if (user.isValid()) {

                HttpSession session = request.getSession(true);
                session.setAttribute("current_session_user", user);
                session.setAttribute("Info", info);
                session.setAttribute("Bookings", bookings);
                if (Objects.equals(user.getUsername(), "admin")) {
                    response.sendRedirect("/admin.jsp"); // Administrator panel
                }
                else {
                    request.getSession().setAttribute("username", user.getFirst_name());
                    response.sendRedirect("/logged.jsp"); // Log In page
                }

            }
            else {
                response.sendRedirect("/error.jsp"); // Error page
            }

        } catch (Throwable throwable) {
            System.out.println(throwable);
        }

    }
}
