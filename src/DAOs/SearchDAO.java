/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package DAOs;

import Beans.SearchBean;
import DB.ConnectionManager;
import java.sql.*;
import java.util.ArrayList;

public class SearchDAO {

    static Connection currentCon = null;
    static ResultSet rs = null;

    public static SearchBean search(SearchBean bean) {

        Statement stmt = null;
        Statement stmt2 = null;
        ArrayList al = null;
        ArrayList search_array = new ArrayList();
            
        String location = bean.getLocation();
        String street = bean.getStreet();
        String neighbourhood = bean.getNeighbourhood();
        int accomodates = bean.getAccomodates();
        String startDate = bean.getStartDate();
        String endDate = bean.getEndDate();
        int userId = bean.getUserId();
        System.out.println(userId);
        String topResults="";
        int i=0;
        if(userId!=0 && BookDAO.userNullBookings(userId) == false)   //We don't add it to the db if user has at least one booking
            userId = 0;
        
        String searchQuery = "select thumbnail_url,price,room_type,beds,number_of_reviews,review_scores_rating,amenities,id from listings where( street like '%" + street + "%'"
                + "OR neighbourhood like '%" + neighbourhood + "%' )";
        
        if (accomodates > 0) {
            searchQuery = searchQuery + " AND accommodates = " + accomodates;
            searchQuery = searchQuery + " AND EXISTS(SELECT * FROM calendar WHERE listings.id=listing_id AND date = \'" +startDate + "\' )" ;
            searchQuery = searchQuery + " AND EXISTS(SELECT * FROM calendar WHERE listings.id=listing_id AND date = \'" +endDate + "\' )" ;
            searchQuery = searchQuery + " AND NOT EXISTS (SELECT * FROM calendar WHERE listings.id=listing_id AND date >= \'" +startDate + "\' AND date <= \'" +endDate +"\' AND available ='f') ORDER BY price ASC ";
            System.out.println(searchQuery);
        }

        System.out.println("Searching terms: " + location +" " + accomodates);
        
        try {
            currentCon = ConnectionManager.getConnection();
            stmt = currentCon.createStatement();
            rs = stmt.executeQuery(searchQuery);
            
            while (rs.next()) {
                al = new ArrayList();
                
                al.add(rs.getString("thumbnail_url"));
                al.add(rs.getString("price"));
                al.add(rs.getString("room_type"));
                al.add(rs.getInt("beds"));
                al.add(rs.getInt("number_of_reviews"));
                al.add(rs.getFloat("review_scores_rating")/20);
                al.add(rs.getString("amenities"));
                //al.add(rs.getString("name"));
                //al.add(rs.getInt("accommodates"));
                //al.add(rs.getString("host_location"));
                al.add(rs.getInt("id"));
                search_array.add(al);
                if(i++ < 20)
                    topResults+= " " + rs.getInt("id");
            }
            String insertQuery= "INSERT INTO searches (userid,searchString) VALUES ("+ userId + " , \'" +topResults + "\' )";
            System.out.println(insertQuery);
            bean.setSearch_rs(search_array);
            stmt2 = currentCon.createStatement();
            if(userId!=0)
                try {
                    stmt2.executeUpdate(insertQuery);
                } catch (Exception e){
                    System.out.println("Update searches table failed: " + e);
                }

        } catch (Exception ex) {
            System.out.println("Search failed: An Exception has occurred! " + ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (Exception e) {
                }
                rs = null;
            }

            if (stmt != null) {
                try {
                    stmt.close();
                } catch (Exception e) {
                }
                stmt = null;
            }

            if (stmt2 != null) {
                try {
                    stmt2.close();
                } catch (Exception e) {
                }
                stmt = null;
            }

            if (currentCon != null) {
                try {
                    currentCon.close();
                } catch (Exception e) {
                }

                currentCon = null;
            }
        }

        return bean;
    }
}
