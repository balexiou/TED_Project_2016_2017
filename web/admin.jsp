<%--
  Created by IntelliJ IDEA.
  User: charis
  Date: 9/21/2017
  Time: 3:40 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page import="Beans.UserBean" %>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width-device-width, initial-scale1=1">
    <meta name="description" content="Vacation Rentals, Homes, Experiences & Places">
    <meta name="keywords" content="hospitality service, rent, lease, vacation rentals, apartment rentals, homestays, hostel, hostel beds, hotel rooms, experiences, places">
    <meta name="author" content="Babis Alexiou">

    <%
        UserBean info = (UserBean) session.getAttribute("Info");
        UserBean current_user = (UserBean) session.getAttribute("current_user");
        //BookBean bookings = (BookBean) session.getAttribute("Bookings");
        //ArrayList results = bookings.getSearch_rs();
        session.setAttribute("Msg", null);
    %>

    <title>Host & Chill | Admin</title>
    <link rel="shortcut icon"  href="/favicon.ico?" />


    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/footer.css">
    <link rel="stylesheet" href="css/admin.css">
    <link rel="stylesheet" href="css/register.css">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.min.css"/>

    <!-- Google Font -->
    <link href="https://fonts.googleapis.com/css?family=Oxygen" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Raleway" rel="stylesheet">

    <!-- Latest compiled and minified JavaScript -->
    <script src="js/jquery.js"></script>
    <script src="js/jquery.min.js"></script>
    <script src="js/bootsrap.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/user.js"></script>
    <script src="js/validatePassword.js"></script>
</head>
<body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">

<%
    if (info == null || !info.getIsAdmin() || current_user == null) {
%>
<h1 align="center" style="font-family: 'Raleway', sans-serif; color: #fff ">Unauthorized Access</h1>
<%
    }
%>
<%
    if (info.getIsAdmin()){
%>

<style>
    #brand_image {
        height: 30px;
    }

</style>
<!-- Navbat Starts Here -->
<nav class="navbar navbar-default navbar-fixed-top">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="admin.jsp">
                <img id="brand_image" src="images/LOGO.jpg" alt="">
            </a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav navbar-right">
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Welcome, <%= info.getFirst_name() %> <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                        <li><img src="<%=info.getPicture()%>" id="profile_image" class=" img-circle" alt=""> <%= info.getFirst_name() %></li>
                        <li class="divider"></li>
                        <li><a href="admin.jsp"><i class="fa fa-id-card-o" aria-hidden="true"></i> Admin Panel</a></li>
                        <li><a href="MessageServlet"><i class="fa fa-inbox" aria-hidden="true"></i> Inbox</a></li>
                        <li><a href="UnverifiedUsersServlet"><i class="fa fa-users" aria-hidden="true"></i> Verify Users</a></li>
                        <li><a href="adminExportXML.jsp"><i class="fa fa-download" aria-hidden="true"></i> Export XML</a></li>
                        <li class="divider"></li>
                        <li><a href="LogoutServlet/"><i class="fa fa-sign-out " aria-hidden="true"></i> Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
<!-- Navbar ends Here! -->

<div id="fullscreen_bg" class="fullscreen_bg"/>
<div class="container">
    <div class="row">
        <div class="col-lg-5 col-md-12 col-sm-8 col-xs-9 bhoechie-tab-container">
            <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 bhoechie-tab-menu">
                <ul class="list-group">
                    <a href="#" class="list-group-item active">
                        <br/><br/><i class="fa fa-user" aria-hidden="true"></i> About<br/><br/>
                    </a>
                    <a href="#" class="list-group-item ">
                        <br/><br/><i class="fa fa-inbox" aria-hidden="true"></i> Inbox<br/><br/>
                    </a>
                    <a href="#" class="list-group-item ">
                        <br/><br/><i class="fa fa-users" aria-hidden="true"></i> Verify Users<br/><br/>
                    </a>
                    <a href="#" class="list-group-item ">
                        <br/><br/><i class="fa fa-download" aria-hidden="true"></i> Export XML<br/><br/>
                    </a>
                    <a href="#" class="list-group-item">
                        <br/><br/><i class="fa fa-cogs" aria-hidden="true"></i> Settings<br/><br/>
                    </a>
                </ul>
            </div>
            <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab">
                <!-- flight section -->
                <div class="bhoechie-tab-content active">
                    <center>
                        <img src="<%=info.getPicture()%>" id="user_profile_image" class="img-circle" alt=""> <br>
                        <hr/>
                        <h2 style="margin-top: 5px;color:#00001a">Welcome</h2>
                        <h3 style="margin-top: 0;color:#00001a">
                            <%=info.getFirst_name()%> <%=info.getLast_name()%>
                        </h3>
                        <%
                            if (info.isVerified()) {
                        %>
                        <i class="fa fa-check-circle fa" aria-hidden="true" style="color: #3CB371;"></i>
                        <%
                        }
                        else {
                        %>
                        <p><i class="fa fa-check-circle-o fa" aria-hidden="true" style="color: #fa4646"></i></p>
                        <%
                            }
                        %>
                        <hr/>
                        <h5 align="center">My Information</h5>
                        <hr/>
                        <p align="left">Username: <%=info.getUsername()%></p>
                        <p align="left">E-mail: <%=info.getEmail()%></p>
                        <p align="left">Phonenumber: <%=info.getPhonenumber()%></p>
                    </center>
                </div>


                <div class="bhoechie-tab-content">
                    <center>
                        <i class=" fa fa-inbox" aria-hidden="true" style="font-size:12em;color:#e17366"></i>
                        <h2 style="margin-top: 0;color:#00001a">Inbox</h2>
                        <hr/>
                    </center>
                </div>

                <div class="bhoechie-tab-content">
                    <center>
                        <i class=" fa fa-users" aria-hidden="true" style="font-size:11em;color:#e17366"></i>
                        <hr/>
                        <h2 style="margin-top: 0;color:#00001a">Verify Users</h2>
                        <hr/>
                        <a href="UnverifiedUsersServlet">
                            <button type="submit" class="btn btn-primary btn-md login-button">Go to User Verification</button>
                        </a>
                    </center>
                </div>

                <div class="bhoechie-tab-content">
                    <center>
                        <i class=" fa fa-download" aria-hidden="true" style="font-size:12em;color:#e17366"></i>
                        <h2 style="margin-top: 0;color:#00001a">Export XML</h2>
                        <hr/>
                        <p>Bookings:</p>
                        <form method="post" action="ExportXMLServlet?btn=1">
                            <center>
                                <button class="btn btn-primary btn-md login-button"><i class="fa fa-download" aria-hidden="true"></i> Download</button>
                            </center>
                        </form>
                        <p>Listings:</p>
                        <form method="post" action="ExportXMLServlet?btn=2">
                            <center>
                                <button class="btn btn-primary btn-md login-button"><i class="fa fa-download" aria-hidden="true"></i> Download</button>
                            </center>
                        </form>
                        <p>Users:</p>
                        <form method="post" action="ExportXMLServlet?btn=3">
                            <center>
                                <button class="btn btn-primary btn-md login-button"><i class="fa fa-download" aria-hidden="true"></i> Download</button>
                            </center>
                        </form>
                    </center>
                </div>

                <div class="bhoechie-tab-content">
                    <center>
                        <i class="fa fa-cogs" aria-hidden="true" style="font-size:12em;color:#e17366"></i>
                        <h2 style="margin-top: 0;color:#00001a">Information Settings</h2>
                        <hr/>
                        <a href="updateuser.jsp">
                            <button type="submit" class="btn btn-primary btn-md login-button">Edit My Information</button>
                        </a>
                    </center>
                </div>
            </div>
        </div>
    </div>
</div>

<%
    }
%>

</body>
</html>
