package Servlets;

import Beans.UserBean;
import DAOs.ListingDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by charis on 9/28/2017.
 */
@WebServlet(name = "ManageListingServlet")
public class ManageListingServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {

            UserBean userBean;
            int user_id;

            HttpSession session = request.getSession(true);

            userBean = (UserBean) session.getAttribute("Info");
            user_id = userBean.getUserID();

            if (userBean.isVerified()){
                ArrayList listings = ListingDAO.getAllListingsFromUser(user_id);

                session.setAttribute("userListings", listings);
            }

            response.sendRedirect("manageListings.jsp");

        } catch (Throwable thException) {
            System.out.println("ManageListingsServlet Failed: " + thException);
        }

    }
}
