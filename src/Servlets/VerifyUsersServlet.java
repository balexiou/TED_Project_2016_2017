package Servlets;

import Beans.UnverifiedUsersBean;
import DAOs.UnverifiedUsersDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by charis on 9/13/2017.
 */
@WebServlet(name = "VerifyUsersServlet")
public class VerifyUsersServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        try {

            UnverifiedUsersBean UsersToVerify = new UnverifiedUsersBean();

            String[] values = request.getParameterValues("UnverifiedUsers");
            List<String> users = new ArrayList<String>(Arrays.asList(values));
            UsersToVerify.setUnverifiedUsers_rs((ArrayList<String>)users);

            UnverifiedUsersDAO.verify_users(UsersToVerify);

            response.sendRedirect("UnverifiedUsersServlet");

        } catch (Throwable thException){
            System.out.println(thException);
        }

    }

}
