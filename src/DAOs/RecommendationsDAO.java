package DAOs;

import Beans.RecommendationsBean;
import DB.ConnectionManager;

import java.awt.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.*;

/**
 * Created by charis on 9/29/2017.
 */
public class RecommendationsDAO {

    static Connection current_connection = null;
    static ResultSet rs = null;

    public static ArrayList<Integer> UserListings(int userid) {

        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        ArrayList<Integer> listings = new ArrayList<Integer>();

        String search_query = "SELECT DISTINCT listing_id FROM bookings WHERE user_id = ?";

        try {
            current_connection = ConnectionManager.getConnection();
            preparedStatement = current_connection.prepareStatement(search_query);
            preparedStatement.setInt(1, userid);
            resultSet = preparedStatement.executeQuery();

            if (!resultSet.next()) {
                search_query = "SELECT DISTINCT listing_id FROM viewedlistings WHERE userid = ?";
                preparedStatement = current_connection.prepareStatement(search_query);
                preparedStatement.setInt(1, userid);
                resultSet = preparedStatement.executeQuery();

               while (resultSet.next()) {
                   System.out.println("Vazw ta viewedlistings!!!!");
                   listings.add(resultSet.getInt("listing_id"));
               }

               search_query = "SELECT DISTINCT searchString FROM  searches WHERE userid = ?";
               preparedStatement = current_connection.prepareStatement(search_query);
               preparedStatement.setInt(1, userid);
               resultSet = preparedStatement.executeQuery();

               String tempString;
               String[] tokens;

               while (resultSet.next()) {
                   System.out.println("Vazw ta searchStrings");
                   tempString = resultSet.getString("searchString");
                   System.out.println("Result: " + tempString);
                   tokens = tempString.split(" ");
                   for (int i = 1; i < tokens.length; i++) {
                       if (tokens[i] == null || tokens[i] == "")
                           break;
                       System.out.println("Integer " + Integer.parseInt(tokens[i]));
                       listings.add(Integer.parseInt(tokens[i]));
                   }
               }
            }
            else {
                resultSet.beforeFirst();
                while (resultSet.next()) {
                    listings.add(resultSet.getInt("listing_id"));
                }
            }
        } catch (Exception ex){
            System.out.println("UserListings Failed: " + ex);
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (Exception e) {
                }
            }

            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }

            if (current_connection != null) {
                try {
                    current_connection.close();
                } catch (Exception e) {
                }
            }
        }

        return listings;
    }

    public static ArrayList<Integer> UserBookedListings(int listingid, int userid) {

        PreparedStatement preparedStatement = null;
        ResultSet resultSet = null;
        ArrayList<Integer> usersArrayList = new ArrayList<Integer>();
        String search_query = "SELECT DISTINCT user_id FROM bookings WHERE listing_id = ? and user_id != ?";

        try {
            current_connection = ConnectionManager.getConnection();
            preparedStatement = current_connection.prepareStatement(search_query);
            preparedStatement.setInt(1, listingid);
            preparedStatement.setInt(2, userid);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                usersArrayList.add(resultSet.getInt("user_id"));
            }
        } catch (Exception ex) {
            System.out.println("UserBookedListings Failed: " + ex);
        } finally {
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (Exception e) {
                }
            }

            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }

            if (current_connection != null) {
                try {
                    current_connection.close();
                } catch (Exception e) {
                }
            }
        }

        return usersArrayList;
    }

    public static RecommendationsBean getRecommendationsInfo(RecommendationsBean recommendationsBean) {

        ArrayList<String> listings_names = new ArrayList<String>();
        ArrayList<Integer> listings_IDs = recommendationsBean.getIDs();
        ArrayList<Float> listings_prices = new ArrayList<Float>();
        ArrayList<String> listings_URLs = new ArrayList<String>();

        Statement statement = null;
        ResultSet resultSet = null;
        String search_query;

        try {

            for (int i = 0; i < listings_IDs.size(); i++) {
                System.out.println("To size einai: " + listings_IDs.size());
                search_query = "SELECT name, thumbnail_url, price FROM listings WHERE id = " + listings_IDs.get(i);
                System.out.println("Getting info for: " + listings_IDs.get(i));
                current_connection = ConnectionManager.getConnection();
                statement = current_connection.createStatement();
                resultSet = statement.executeQuery(search_query);
                resultSet.next();
                listings_names.add(resultSet.getString("name"));
                listings_prices.add(resultSet.getFloat("price"));
                listings_URLs.add(resultSet.getString("thumbnail_url"));
                System.out.println("Info for [" + listings_IDs.get(i) + "]: " + resultSet.getString("name") + ", " + resultSet.getFloat("price") + ", " + resultSet.getString("thumbnail_url"));
            }

            recommendationsBean.setNames(listings_names);
            recommendationsBean.setUrls(listings_URLs);
            recommendationsBean.setPrices(listings_prices);

        } catch (Exception ex) {
            System.out.println("getRecommendationsInfo Failed: " + ex);
        } finally {
            if (statement != null) {
                try {
                    statement.close();
                } catch (Exception e) {
                }
            }

            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (Exception e) {
                }
            }

            if (current_connection != null) {
                try {
                    current_connection.close();
                } catch (Exception e) {
                }
            }
        }

        return recommendationsBean;
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public static RecommendationsBean recommendedListings(int userid, int numberOfListings) {

        RecommendationsBean recListings = null;
        Iterator<Integer> UserListingsIterator;
        int currListId;
        Iterator<Integer> usersIterator;
        int currUserId;
        Iterator<Integer> OtherUserListingsIterator;
        int currListId2;

        HashMap<Integer, Integer> hashMap = new HashMap<Integer, Integer>();
        ArrayList<Integer> UserListings = UserListings(userid);
        HashSet<Integer> UserListingsSet = new HashSet<Integer>(UserListings);
        ArrayList<Integer> UsersOfListing;
        ArrayList<Integer> UserListings2;

        if (UserListings.isEmpty()){
            return null;
        }

        UserListingsIterator = UserListings.iterator();
        while (UserListingsIterator.hasNext()) {
            currListId = UserListingsIterator.next();
            UsersOfListing = UserBookedListings(currListId, userid);
            usersIterator = UsersOfListing.iterator();
            while (usersIterator.hasNext()) {
                currUserId = usersIterator.next();
                if (!hashMap.containsKey(currUserId))
                    hashMap.put(currUserId, 1);
                else
                    hashMap.put(currUserId, hashMap.get(currUserId) + 1);
            }
        }

        Object[] entryArray = hashMap.entrySet().toArray();
        Arrays.sort(entryArray, new Comparator() {
            @Override
            public int compare(Object object1, Object object2){
                return ((Map.Entry<String, Integer>) object2).getValue().compareTo(
                        ((Map.Entry<String, Integer>) object1).getValue());
            }
        });

        int counter = 0;
        int recommendations_found = 0;
        int users_to_check_from = 8;

        ArrayList<Integer> recommended = new ArrayList<Integer>();

        for (Object object : entryArray) {
            if (users_to_check_from == counter || (recommendations_found == numberOfListings))
                break;
            UserListings2 = UserListings(((Map.Entry<Integer, Integer>) object).getKey());
            OtherUserListingsIterator = UserListings2.iterator();
            while (OtherUserListingsIterator.hasNext()) {
                currListId2 = OtherUserListingsIterator.next();
                if (!recommended.contains(currListId2)) {
                    recommended.add(currListId2);
                    if (++recommendations_found == numberOfListings)
                        break;
                }
            }
            counter++;
        }

        if (recommended.isEmpty())
            return null;
        recListings = new RecommendationsBean();
        recListings.setIDs(recommended);
        recListings = getRecommendationsInfo(recListings);

        return recListings;
    }
}
